<?php

namespace App\Http\Controllers;

use App\Models\itinerary;
use Illuminate\Http\Request;

class ItineraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function show(itinerary $itinerary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function edit(itinerary $itinerary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, itinerary $itinerary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function destroy(itinerary $itinerary)
    {
        //
    }
}
