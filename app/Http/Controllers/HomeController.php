<?php

namespace App\Http\Controllers;

use App\Models\attraction;
use App\Models\itinerary;
use App\Models\package;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $packages = package::all();
        return view('home')
            ->with('packages',$packages);
    }
    public function details($id){
        $package =package::select('packages.*','types.name as Name')
            ->leftJoin('types', 'types.id', 'packages.type_id')->where('packages.id',$id)->first();

        //Includes
        $includes = explode('.',$package->includes);
        //Excludes
        $excludes = explode('.',$package->excludes);

        // Attractions
        $attractions = attraction::where('package_id',$package->id)->get();
        $att_titles = [];
        foreach ($attractions as $item){
            array_push($att_titles,$item->title);
        }
        $att_titles= array_unique($att_titles);

        // Itineraries
        $iti = itinerary::where('package_id',$id)->get();


        return view('details')->with('package',$package)
            ->with('includes',$includes)
            ->with('excludes',$excludes)
            ->with('titles',$att_titles)
            ->with('attraction',$attractions)
            ->with('itineraries',$iti);
    }
}
