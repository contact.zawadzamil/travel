<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 *
 * @OA\Info(
 *      version="v1",
 *      title="Test API's",
 *      description="These API's are to test Swagger @ZAAG",
 *      @OA\Contact(
 *          email="contact.zawadzamil@gmail.com"
 *      )
 * )
 * @OA\Server(
 *      url= L5_SWAGGER_CONST_HOST,
 *      description="*** API Server"
 * )
 * @OA\SecurityScheme(
 *     type="http",
 *     description="API token is required to access this API",
 *     in="header",
 *     scheme="bearer",
 *     securityScheme="bearerAuth",
 * )
 *
 */
class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
