<?php

namespace App\Http\Controllers;

use App\Models\attraction;
use App\Models\Message;
use App\Models\package;
use Illuminate\Http\Request;
use Nette\Utils\Type;

class DashboardController extends Controller
{
    public function getPackages(){
        $types = \App\Models\type::all();
        return view('packages')
            ->with('types',$types);
    }
    public function adminDashboard(){
        $packages = package::all();
        $messages = Message::all();
        $attractions = attraction::all()->count();
        return view('admin.dashboard')->with('packages',$packages)
            ->with('messages',$messages)
            ->with('attractions',$attractions);
    }

    public function pushIndex(){
        return view('admin.pusher.pusher');
    }


}
