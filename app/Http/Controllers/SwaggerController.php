<?php

namespace App\Http\Controllers;

use App\Models\attraction;
use App\Models\itinerary;
use App\Models\package;
use App\Models\type;
use Illuminate\Http\Request;

class SwaggerController extends Controller
{
    /**
     * @OA\Get(
     *     path="/swagger",
     *     tags={"package"},
     *     summary="Finds Packages by status",
     *     description="Multiple status values can be provided with comma separated string",
     *     operationId="index",
     *
     *     @OA\Parameter(
     *         name="",
     *         in="query",
     *         description="Status values that needed to be considered for filter",
     *         required=true,
     *         explode=true,
     *         @OA\Schema(
     *
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid status value"
     *     ),
     *
     * )
     */
    public function index()
    {
        $packages  =  package::all();
        return response()->json($packages);
    }
    /**
     * @OA\Post(
     * path="/add_type",
     * summary="Add Type",
     * description="Add New Type",
     * operationId="addType",
     * tags={"type"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass type fildes",
     *    @OA\JsonContent(
     *       required={"name","description"},
     *       @OA\Property(property="name", type="string", example="Dhaka Tour"),
     *       @OA\Property(property="description", type="string",  example="Lorem Ipsum "),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     ),
     *  @OA\Response(
     *         response=200,
     *         description="successful operation",
     *
     *     ),
     * )
     */
    public function storeType(Request $request){
        // Store Type
        $type = type::create([
            'name' =>$request->name,
            'description'=>$request->description
        ]);
        $type->save();
        $types = type::all();
        return response() ->json(['success'=>'true','data'=>$type]);
    }


    /**
     * @OA\Post(
     * path="/edit_type/{id}",
     * summary="Add Type",
     * description="Add New Type",
     * operationId="editType",
     * tags={"type"},
     *     @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass type fildes",
     *    @OA\JsonContent(
     *       required={"name","description"},
     *       @OA\Property(property="name", type="string", example="Dhaka Tour"),
     *       @OA\Property(property="description", type="string",  example="Lorem Ipsum "),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     ),
     *  @OA\Response(
     *         response=200,
     *         description="successful operation",
     *
     *     ),
     * )
     */
    public function editType(Request $request,$id){
        $type = type::find($id);
        $type->update([
            'name'=>$request->input('name'),
            'description'=>$request->input('description')
        ]);
      return response() ->json(['success'=>'true','data'=>$type]);
    }
}
