<?php

namespace App\Http\Controllers;

use App\Models\attraction;
use App\Models\itinerary;
use App\Models\package;
use App\Models\Schedule;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = package::select('packages.*','types.name as Name')
                           ->leftJoin('types', 'types.id', 'packages.type_id')
                           ->orderBy('packages.created_at', 'desc')->get();

       return view('admin.packages')->with('packages',$packages);

 }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


       // Storing Package

        //Image Save
        $request->validate([
            'image' => 'required|image|mimes:png,jpg,jpeg|max:2048'
        ]);
        $imageName = time().rand().'.'.$request->image->extension();
        $request->image->storeAs('images', $imageName);


        $package = package::create([
            'title'=>$request->input('title'),
            'type_id'=>$request->input('type'),
            'duration'=>$request->input('duration'),
            'price'=>$request->input('price'),
            'details'=>$request->input('details'),
            'location'=>$request->input('location'),
            'tour_available'=>$request->input('tour_available'),
            'best_time'=>$request->input('best_time'),
            'tour_highlights'=>$request->input('tour_highlight'),
            'includes'=>$request->input('includes'),
            'excludes'=>$request->input('excludes'),
            'notes'=>$request->input('notes'),
            'status'=>$request->input('status'),
            'image'=>$imageName,
        ]);
        $package->save();


        // Storing Attraction Data
        $reAttraction = $request ->attraction_array;
        foreach ($reAttraction as $item){
           $item = json_decode($item);

            foreach ($item as $i){
                $attraction = attraction::create([
                    'title'=>$i->title,
                    'details'=>$i->details,
                    'package_id'=>$package->id,

                ]);
                $attraction->save();
            }

        }



        // Storing Itinerary Data
        $reIti = $request ->itinerary_array;
        foreach ($reIti as $item){
            $item = json_decode($item);
            foreach ($item as $i){
                $itinerary = itinerary::create([
                    'day'=>$i->day,
                    'title'=>$i->title,
                    'details'=>$i->details,
                    'package_id'=>$package->id,
                ]);
                $itinerary->save();
            }

        }

        $packages = package::select('packages.*','types.name as Name')
            ->leftJoin('types', 'types.id', 'packages.type_id')
            ->orderBy('packages.created_at', 'desc')->get();


        return redirect()->route('packages')->with('success','Data Inserted Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\package  $package
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package =package::select('packages.*','types.name as Name')
            ->leftJoin('types', 'types.id', 'packages.type_id')->where('packages.id',$id)->first();

        // Attractions
        $attractions = attraction::where('package_id',$package->id)->get();
        $att_titles = [];
        foreach ($attractions as $item){
            array_push($att_titles,$item->title);
        }
        $att_titles= array_unique($att_titles);

        //Itinereries
        $ites = itinerary::where('package_id',$package->id)->get();

        //Includes
        $includes = explode('.',$package->includes);

        //Excludes
        $excludes = explode('.',$package->excludes);



        return view('admin.single-package')->with('package',$package)
            ->with('attractions',$attractions)
            ->with('titles',$att_titles)
            ->with('ities',$ites)
            ->with('includes',$includes)
            ->with('excludes',$excludes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package =package::select('packages.*','types.name as Name')
            ->leftJoin('types', 'types.id', 'packages.type_id')->where('packages.id',$id)->first();

        $attractions = attraction::where('package_id',$id)->get();
        $itis = itinerary::where('package_id',$id)->get();
        $itis = json_encode($itis);
        $attractions = json_encode($attractions);
        return view('admin.edit-package')->with('package',$package)
            ->with('attractions',$attractions)
            ->with('itis',$itis);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {


       $package  = package::find($id);
       if(isset($request->image)){
           $request->validate([
               'image' => 'required|image|mimes:png,jpg,jpeg|max:2048'
           ]);
           $imageName = time().rand().'.'.$request->image->extension();
           $request->image->storeAs('images', $imageName);
       }
       else{
           $imageName = $package->image;
       }
       $package->update([
           'title'=>$request->input('title'),
           'type_id'=>$request->input('type'),
           'duration'=>$request->input('duration'),
           'price'=>$request->input('price'),
           'details'=>$request->input('details'),
           'location'=>$request->input('location'),
           'tour_available'=>$request->input('tour_available'),
           'best_time'=>$request->input('best_time'),
           'tour_highlights'=>$request->input('tour_highlight'),
           'includes'=>$request->input('includes'),
           'excludes'=>$request->input('excludes'),
           'notes'=>$request->input('notes'),
           'status'=>$request->input('status'),
           'image'=>$imageName,

       ]);
       // Delete All existing Attractions
       attraction::where('package_id',$id)->delete();
       // Delete All Existing Initnarary
        itinerary::where('package_id',$id)->delete();


        // Storing Attraction Data
        $reAttraction = $request ->attraction_array;
        foreach ($reAttraction as $item){
            $item = json_decode($item);

            foreach ($item as $i){
                $attraction = attraction::create([
                    'title'=>$i->title,
                    'details'=>$i->details,
                    'package_id'=>$id,

                ]);
                $attraction->save();
            }

        }

        // Storing Itinerary Data
        $reIti = $request ->itinerary_array;
        foreach ($reIti as $item){
            $item = json_decode($item);
            foreach ($item as $i){
                $itinerary = itinerary::create([
                    'day'=>$i->day,
                    'title'=>$i->title,
                    'details'=>$i->details,
                    'package_id'=>$id,
                ]);
                $itinerary->save();
            }

        }
        $packages = package::select('packages.*','types.name as Name')
            ->leftJoin('types', 'types.id', 'packages.type_id')
            ->orderBy('packages.created_at', 'desc')->get();

       return redirect()->route('packages')->with('success','Data Updated Successfylly')
           ->with('packages',$packages);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        package::where('id',$id)->delete();
        attraction::where('package_id',$id)->delete();
        itinerary::where('package_id',$id)->delete();
        Schedule::where('package_id',$id)->delete();
        return redirect()->back()->with('success','Data Removed Successfully');
    }

    // API Functions
    public function getPackages(){
        $packages = package::all();
        return response()->json($packages);
    }
    public function  getDuration($id){
        $package = package::find($id);
        $duration = $package->duration;
        return response()->json($duration);
    }
}
