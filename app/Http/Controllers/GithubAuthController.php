<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;

class GithubAuthController extends Controller
{
    public function redirect(){
        return Socialite::driver('github')->redirect();
    }
    public function callbackGithub()
    {
        try {
            $github_user = Socialite::driver('github')->user();

            $user = User::where('github_id',$github_user->getId())->get();


            if(count($user)==0){


                DB::table('users')->insert([
                    'name' => $github_user->getName(),
                    'email' => $github_user->getEmail(),
                    'github_id' => $github_user->getId(),
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                ]);
                $new_user= User::where('github_id',$github_user->getId())->first();
                Auth::login($new_user);
                return redirect()->intended(RouteServiceProvider::HOME);
            }
            else{
                $new_user= User::where('github_id',$github_user->getId())->first();
                Auth::login($new_user);

                return redirect()->intended(RouteServiceProvider::HOME);

            }

        }
        catch (\Throwable $th){
            $message = 'An Error Found!';
            if(str_contains($th->getMessage(),'Duplicate entry')){
                $message  = 'Email Already Registered';
            }
            return redirect()->route('login')->with('status',$message);
        }
    }

    public function savePassword(Request $request){



    }
}
