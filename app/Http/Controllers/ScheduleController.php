<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::all();
        return view('admin.schedule.schedule')->with('schedules',$schedules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schedule.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule = Schedule::create([
            'start'=>$request->input('start'),
            'end'=>$request->input('end'),
            'package_id'=>$request->input('package'),
            'status'=>$request->input('status'),
        ]);
        $schedule->save();
        $schedules = Schedule::all();
        return  redirect()->route('schedule.index')->with('success','Schedule Added!')
            ->with('schedules',$schedules);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedule = Schedule::find($id);
        return view('admin.schedule.edit')->with('schedule',$schedule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $schedule = Schedule::find($id);
        $schedule->update([
            'package_id'=>$request->package,
            'start'=>$request->input('start'),
            'end'=>$request->input('end'),
            'status'=>$request->input('status'),
        ]);
        $schedules = Schedule::all();
        return redirect()->route('schedule.index')->with('success','Schedule Updated Successfully')
            ->with('schedules',$schedules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Schedule::where('id',$id)->delete();
        return redirect()->back()->with('success','Schedule Removed Successfully');
    }
}
