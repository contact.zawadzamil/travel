<?php

namespace App\Http\Controllers;

use App\Events\PusherTestEvent;
use Illuminate\Http\Request;

class EventController extends Controller
{

    public function pushTestMessage(Request $request){

        event(new PusherTestEvent($request->message));
        return redirect()->back()->with('success','Notification Sent to all Users');
    }
}
