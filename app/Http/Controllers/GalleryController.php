<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\type;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::all();
        return view('admin.gallery.galleries')->with('galleries',$galleries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $galleries = Gallery::all();
        $count = $galleries->count();
       return view('admin.gallery.add')->with('galleries',$galleries)
           ->with('count',$count);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//      foreach($request->image as $item){
//          $allowedfileExtension=['jpeg','jpg','png'];
//          $extension = $item->extension();
//
//          $check=in_array($extension,$allowedfileExtension);
//          if ($check){
//              $imageName = time().rand().'.'.$item->extension();
//              $item->storeAs('images', $imageName);
//              $gallery = Gallery::create([
//                  'image'=>$imageName,
//                  'package_id'=>$request->package
//              ]);
//              $gallery ->save();
//          }
//          else{
//              return redirect()->back()->with('failed','Sorry! Only PNG, JPG,JPEG are allowed');
//          }
//      }
//      $galleries = Gallery::all();
//        $count = $galleries->count();
//      return redirect()->back()->with('success','Gallery Uploaded Successfully')->with('galleries',$galleries)
//          ->with('count',$count);

        $request->validate([
            'image' => 'required|image|mimes:png,jpg,jpeg|max:2048'
        ]);
        $imageName = time().rand().'.'.$request->image->extension();
        $request->image->storeAs('images', $imageName);
        $gallery = Gallery::create([
            'image'=>$imageName,
            'package_id'=>$request->package
        ]);
        $gallery->save();
        return redirect()->back()->with('success','Image Added to Gallery');

//            'image' => 'required|image|mimes:png,jpg,jpeg|max:2048'
//        ]);

//        $imageName = time().rand().'.'.$request->image->extension();
//        $request->image->storeAs('images', $imageName);
//        $gallery =

//        foreach ($request->image as $item){
//            $item->validate([
//            'image' => 'required|image|mimes:png,jpg,jpeg|max:2048'
//        ]);
//        $imageName = time().rand().'.'.$item->image->extension();
//            dd($imageName);
//
//        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gallery::where('id',$id)->delete();
        return redirect()->back()->with('success','Data Removed Successfully');
    }
}
