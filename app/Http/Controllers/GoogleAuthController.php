<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;

class GoogleAuthController extends Controller
{
    public function redirect(){
        return Socialite::driver('google')->redirect();
    }
    public function callbackGoogle()
    {
        try {
            $google_user = Socialite::driver('google')->user();

            $user = User::where('google_id',$google_user->getId())->get();


            if(count($user)==0){


               DB::table('users')->insert([
                    'name' => $google_user->getName(),
                    'email' => $google_user->getEmail(),
                    'google_id' => $google_user->getId(),
                   "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                   "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                    ]);
                $new_user= User::where('google_id',$google_user->getId())->first();
                Auth::login($new_user);
                return redirect()->intended(RouteServiceProvider::HOME);
            }
            else{
                $new_user= User::where('google_id',$google_user->getId())->first();
                Auth::login($new_user);

                return redirect()->intended(RouteServiceProvider::HOME);

            }

        }
        catch (\Throwable $th){
            $message = 'An Error Found!';
            if(str_contains($th->getMessage(),'Duplicate entry')){
                $message  = 'Email Already Registered';
            }
        return redirect()->route('login')->with('status',$message);
        }
    }

    public function savePassword(Request $request){



    }
}
