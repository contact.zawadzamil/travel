<?php

namespace App\Http\Controllers;

use App\Models\package;
use App\Models\type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = type::all();
        return response()->json($types);
    }
    public function indexWeb()
    {
        $types = type::all();
        return view('admin.type.types')->with('types',$types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.type.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $type = type::create([
           'name' =>$request->name,
           'description'=>$request->description
       ]);
       $type->save();
       $types = type::all();
       return redirect()->route('type.index-web')->with('types',$types)
           ->with('success','Type Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(type $type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = type::find($id);
        return view('admin.type.edit')->with('type',$type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = type::find($id);
        $type->update([
            'name'=>$request->input('name'),
            'description'=>$request->input('description')
        ]);
        $types = type::all();
        return redirect()->route('type.index-web')
            ->with('success','Type Updates Successfully')
            ->with('types',$type);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = package::where('type_id',$id)->get();
        $packageArray = [];
        foreach ($package as $item){
            array_push($packageArray,$item->title);
        }
        $packageArray = json_encode($packageArray);
        $package_count = package::where('type_id',$id)->count();
        if($package_count>=1){
            return redirect()->back()->with('failed','The Following Type is Already Used in Package(s) Name:'.$packageArray.' So it Cant be Removed');
        }
        else{
            type::where('id',$id)->delete();
            return redirect()->back()->with('success','Type Removed Successfully');
        }

    }
}
