<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;
    protected $fillable = [
        'start',
        'end',
        'package_id',
        'status',

    ];

    public function package(){
        return $this->belongsTo(package::class);
    }
}
