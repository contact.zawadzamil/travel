<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class package extends Model
{
    use HasFactory;
    protected $fillable =[
        'title',
        'image',
        'type_id',
        'duration',
        'details',
        'location',
        'tour_available',
        'best_time',
        'tour_highlights',
        'includes',
        'excludes',
        'notes',
        'status',
        'price',

    ];
    public function gallery(){
        return  $this ->hasMany(Gallery::class);
    }

    public function schedule(){
        return $this ->hasMany(Schedule::class);
    }
    public function type(){
        return $this ->belongsTo(type::class);
    }

}
