
// ADD Attraction
if($('#attraction_array').val()=='' ||$('#attraction_array').val()==null ){
    var arr = [];
    let count = 0;
    $('#add_attraction').click(function (e){
        e.preventDefault();
        const title = $('#attraction_title').val();
        const details = $('#attraction_details').val();
        count = count+1;
        const attraction = {id:count,title:title,details:details}
        arr.push(attraction)
        $('#attraction_array').val(JSON.stringify(arr));

        $('#tbody').append(`<tr id="${count}">
    <td>
        ${count}
    </td>
          <td >
               ${title}
          </td>
           <td >
            ${details}
            </td>
            <td>
            <button class="btn btn-danger remove_attr" id="${count}">Remove</button>
</td>
           </tr>`);

        $('#attraction_title').val('');
        $('#attraction_details').val('');

    });
}
else{
    var arr = JSON.parse($('#attraction_array').val());
    let count = 0;
    arr.forEach((element)=>{
        $('#tbody').append(`<tr id="${element.id}">
    <td>
        ${element.id}
    </td>
          <td >
               ${element.title}
          </td>
           <td >
            ${element.details}
            </td>
            <td>
            <button class="btn btn-danger remove_attr" id="${element.id}">Remove</button>
</td>
           </tr>`);
        count = element.id;
    })

    $('#add_attraction').click(function (e){
        e.preventDefault();
        const title = $('#attraction_title').val();
        const details = $('#attraction_details').val();
        count = count+1;
        const attraction = {id:count,title:title,details:details}
        arr.push(attraction)
        $('#attraction_array').val(JSON.stringify(arr));

        $('#tbody').append(`<tr id="${count}">
    <td>
        ${count}
    </td>
          <td >
               ${title}
          </td>
           <td >
            ${details}
            </td>
            <td>
            <button class="btn btn-danger remove_attr" id="${count}">Remove</button>
</td>
           </tr>`);

        $('#attraction_title').val('');
        $('#attraction_details').val('');

    });
}

//Remove Attraction
$(document).on('click','.remove_attr',function(event){
    event.preventDefault();
   // console.log(event.target.id)


    arr = arr.filter(item =>item.id != event.target.id);
    $(this).closest('tr').remove();
    $('#attraction_array').val(JSON.stringify(arr));
    console.log(arr)

});


//Add Itenary
if($('#itinerary_array').val()=='' || $('#itinerary_array').val()==null){
    let tour_i_count = 0;
    var tour_i_arr = [];
    $('#add_itinerary').click(function (e){
        e.preventDefault();
        const title = $('#tour_itinerary_title').val();
        const details = $('#tour_itinerary_details').val();
        const day = $('#tour_itinerary_day').val();
        if(title!='' && details !=''){
            tour_i_count +=1;
            const itinerary = {id:tour_i_count,title:title,details:details,day:day}
            tour_i_arr.push(itinerary);
            $('#itinerary_array').val(JSON.stringify(tour_i_arr));

            $('#itinerary_tbody').append(`<tr id="${tour_i_count}">
    <td>
        ${tour_i_count}
    </td>
            <td>
            ${day}
        </td>
          <td >
               ${title}
          </td>
           <td >
            ${details}
            </td>
            <td>
            <button class="btn btn-danger remove_attr_it" id="${tour_i_count}">Remove</button>
</td>
           </tr>`);
        }
    });
}
else{
    let tour_i_count = 0;
    var tour_i_arr = JSON.parse($('#itinerary_array').val());
    tour_i_arr.forEach((element)=>{
        $('#itinerary_tbody').append(`<tr id="${element.id}">
    <td>
        ${element.id}
    </td>
            <td>
            ${element.day}
        </td>
          <td >
               ${element.title}
          </td>
           <td >
            ${element.details}
            </td>
            <td>
            <button class="btn btn-danger remove_attr_it" id="${element.id}">Remove</button>
</td>
           </tr>`);
        tour_i_count = element.id;
    })
    $('#add_itinerary').click(function (e){
        e.preventDefault();
        const title = $('#tour_itinerary_title').val();
        const details = $('#tour_itinerary_details').val();
        const day = $('#tour_itinerary_day').val();
        if(title!='' && details !=''){
            tour_i_count +=1;
            const itinerary = {id:tour_i_count,title:title,details:details,day:day}
            tour_i_arr.push(itinerary);
            $('#itinerary_array').val(JSON.stringify(tour_i_arr));

            $('#itinerary_tbody').append(`<tr id="${tour_i_count}">
    <td>
        ${tour_i_count}
    </td>
            <td>
            ${day}
        </td>
          <td >
               ${title}
          </td>
           <td >
            ${details}
            </td>
            <td>
            <button class="btn btn-danger remove_attr_it" id="${tour_i_count}">Remove</button>
</td>
           </tr>`);
        }
    });

}
$(document).on('click','.remove_attr_it',function(event){

    $(this).closest('tr').remove();
    tour_i_arr= tour_i_arr.filter(item => item.id != event.target.id);
    $('#itinerary_array').val(JSON.stringify(tour_i_arr));
    console.log(tour_i_arr)

});

// Fetch Types From Database
$(document).ready(function (){
    $.ajax({
        url: "http://127.0.0.1:8000/api/get_types",
        type: 'GET',
        dataType: 'json', // added data type
        success: function(res) {

            res.forEach((element)=>{
                $('#typeSelect').append(`<option value="${element.id}">
                                       ${element.name}
                                  </option>`);
            })


        }
    });
})

// Show Image Preview
$('#image').change(function (event){
   if(event.target.files.length >0){
       var src = URL.createObjectURL(event.target.files[0]);
       var preview = document.getElementById("preview_image");
       var previewDiv = document.getElementById("imagePreview");
        preview.src = src;
       previewDiv.style.display = "block";
   }
});
// Cross Image
$('#cross_image').click(function (){

    var previewDiv = document.getElementById("imagePreview");
    previewDiv.style.display = "none";
    $('#image').val('');
})
// Status Select
if($('#response_status').val()!='' || $('#response_status').val()!=null){
    $('.status').each(function (){
        if($(this).val()==$('#response_status').val()){
            $(this).attr('checked', true);
        }
    })
}



