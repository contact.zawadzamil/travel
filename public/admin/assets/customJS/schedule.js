
$('#send').click(function (e){
    e.preventDefault();
    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    if(startDate > endDate){
        alert('Start Date Cannot be Grater Than End Date')
    }
    else{
        document.forms["myform"].submit();
    }
})
$('.duration_div').hide();

$(document).ready(function (){
    $.ajax({
        url: "http://127.0.0.1:8000/api/get_packages",
        type: 'GET',
        dataType: 'json', // added data type
        success: function(res) {

            res.forEach((element)=>{
                $('#PackageSelectSc').append(`<option value="${element.id}">
                                       ${element.title}
                                  </option>`);
            })



        }
    });
})

$('#PackageSelectSc').change(function (e){
    const id = e.target.value;
    $.ajax({
        url: "http://127.0.0.1:8000/api/duration/"+id,
        type: 'GET',
        dataType: 'json', // added data type
        success: function(res) {
            $('.duration_div').show();

           $('#duration').empty();
           $('#duration').text(res);
        }
    });
})

// Status Select
if($('#response_status_sc').val()!='' || $('#response_status_sc').val()!=null){
    $('.status').each(function (){
        if($(this).val()==$('#response_status_sc').val()){
            $(this).attr('checked', true);
        }
    })
}
