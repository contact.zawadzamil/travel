@extends('layout')
@section('body_parts')
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Tour Packages</h2>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
		<div class="container content">
        <!-- Service Blcoks -->
            @foreach($types as $type)
                <div class="row type_row">
                    <h2 class="type_title">{{$type -> name}}</h2>
                    <p class="type_details">
                       {{$type ->description}}

                    </p>
                    <hr>
                    @foreach($type -> packages as $item)
                        <a href="{{route('home.details',$item->id)}}">
                            <div class="package">
                                <div class="package_image">
                                    <img src="{{'storage/app/images/'.$item->image}}" alt="">
                                </div>
                                <div class="others">
                                    <div class="package_title">
                                        <h4>{{$item->title}}</h4>
                                        <div class="duration">{{$item->duration}}</div>
                                    </div>
                                    <div class="package_details">
                                        <p>{{$item->details}}</p>
                                    </div>
                                    <div class="button">
                                        <button class="btn green">Show Itinerary</button>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            @endforeach




        <!-- End Service Blcoks -->




    </div>
    </section>
    @endsection
