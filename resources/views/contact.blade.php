@extends('layout')
@section('body_parts')
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle">Contact Us</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about-logo contact-details">
                        <h3>Contact US</h3>
                        <ul>
                            <li>
                                <strong>Bangladesh Office: </strong>82/4 (Gr. Floor), Ibrahimpur, Kafrul, Dhaka-1206
                            </li>
                            <li>
                                <strong> France Office:</strong> 92 Rue de Maubeuge, 75010 Paris.
                            </li>
                            <li><strong>Phone & WhatsApp:</strong>

                                +8801791005909, +880 191 339 4033,

                                +88 015 1717 1085. +33 6 05 69 49 85.
                            </li>
                            <li><strong>E-mail: </strong>info@pathfriend-bd.com, pathfriendbd@gmail.com</li>
                        </ul>


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!-- Form itself -->
                    <form name="sentMessage" action="{{route('message.store')}}" id="contactForm" method="POST" >
                        @csrf
                        <h3>Western Tour Operator</h3>
{{--                        //Input Name --}}
                        <div class="control-group">
                            <div class="controls">
                                <input type="text" class="form-control"
                                       placeholder="Full Name " id="name" required name="name"
                                       />
                                <p class="help-block"></p>
                            </div>
                        </div>

{{--                        Input Email --}}
                        <div class="control-group">
                            <div class="controls">
                                <input type="email" class="form-control" placeholder="Email"
                                       id="email" required name="email"
                                       data-validation-required-message="Please enter your email"/>
                            </div>
                        </div>

{{--                        Input Subject --}}
                        <div class="control-group">
                            <div class="controls">
                                <input type="text" class="form-control" placeholder="Enter Subject"
                                       id="subject" required name="subject"
                                       data-validation-required-message="Please enter your Subject"/>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
				 <textarea rows="10" cols="100" class="form-control" name="message"
                           placeholder="Message" id="message" required
                           data-validation-required-message="Please enter your message" minlength="5"
                           data-validation-minlength-message="Min 5 characters"
                           maxlength="999" style="resize:none"></textarea>
                            </div>
                        </div>
                        <div id="success">
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                        </div> <!-- For success/fail messages -->
                        <button type="submit" class="btn btn-primary pull-right">Send</button>
                        <br/>
                    </form>
                </div>
                <div class="col-md-6">
                   <div class="cover_image">
                       <img src="{{asset('storage/images/winter.jpg')}}" alt="">
                   </div>
                </div>
            </div>
        </div>

    </section>
@endsection
