<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Western Multi purpose Free HTML5 Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content="http://webthemez.com"/>
    <!-- css -->
    <link href="public/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="public/assets/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="public/assets/css/flexslider.css" rel="stylesheet"/>
    <link href="public/assets/css/style.css" rel="stylesheet"/>
    <link href="public/assets/css/custom.css" rel="stylesheet"/>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

{{--    Pusher --}}
    <script src="https://js.pusher.com/7.2/pusher.min.js"></script>
    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('bc0c4e3c10fd5a25c02f', {
            cluster: 'ap2'
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            alert(JSON.stringify(data));
        });
    </script>
</head>
<body>
<div id="wrapper" class="home-page">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="pull-left hidden-xs"><i class="fa fa-clock-o"></i><span>Mon - Sat 8.00 - 18.00. Sunday CLOSED</span>
                    </p>
                    <p class="pull-right"><i class="fa fa-phone"></i>Tel No. (+001) 123-456-789</p>
                </div>
            </div>
        </div>
    </div>
    <!-- start header -->
    <header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('')}}"><img src="public/assets/img/logo.png" alt="logo"/></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{url('')}}">Home</a></li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Know <b
                                    class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('meet')}}">Meet Bangladesh</a></li>
                                <li><a href="{{url('history')}}">History of Bangladesh</a></li>
                                <li><a href="{{url('language')}}">Language, Culture & Religion of Bangladesh</a></li>
                                <li><a href="{{url('geography')}}">Geography and Climate of Bangladesh</a></li>
                            </ul>
                        </li>
                        <li><a href="{{url('tour_packages')}}">Tour Packages</a></li>
                        <li><a href="{{url('services')}}">Services</a></li>
                        <li><a href="{{url('why-us')}}">Why Us</a></li>
                        <li><a href="{{url('contact')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </header>
    <!-- end header -->
    <section id="banner">

        <!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
                <li>

                    <img src="{{asset('storage/images/DQVK-QDXUAAM8GP.jpg')}}" alt=""/>
                    <div class="flex-caption">
                        <h3>Bangladesh is open for travel.</h3>
                        <p>Explore the Land of River</p>

                    </div>
                </li>
                <li>

                    <img src="{{asset('storage/images/ChandranathHillandMandir2.jpeg')}}" alt=""/>
                    <div class="flex-caption">
                        <h3>Bangladesh is open for travel.</h3>
                        <p>Discover the mystery of magical hills.</p>

                    </div>
                </li>
                <li>

                    <img src="{{asset('storage/images/Sundarbans-ds.jpg')}}" alt=""/>
                    <div class="flex-caption">
                        <h3>Bangladesh is open for travel.</h3>
                        <p>Walk thorugh the history & culture.</p>

                    </div>
                </li>
                <li>

                    <img src="{{asset('storage/images/wp3784701.jpg')}}" alt=""/>
                    <div class="flex-caption">
                        <h3>Bangladesh is open for travel.</h3>
                        <p>Live in the green, fill the soul.</p>

                    </div>
                </li>
            </ul>
        </div>
        <!-- end slider -->

    </section>
    <section id="call-to-action-2">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-9">
                    <h3>Leading tour operator in Bangladesh!</h3>
                    <p>Western Tour Operator is an award-winning local tour operator & your travel solution in Bangladesh determined to provide a unique and satisfying travel experience. Our high qualified services, comfortable tour planning, guides with spacious knowledge of local information and the affordable price will make your trip enjoyable and special. If you want to explore Bangladesh completely rich, we are here with experiences to make it memorable.</p>
                </div>
                <div class="col-md-2 col-sm-3">
                    <a href="{{url('tour_packages')}}" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
    </section>

    <section id="content">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aligncenter"><h2 class="aligncenter">Our Best Offers</h2>
                    </div>
                    <br/>
                </div>
            </div>

            <div class="row box-section">
                @foreach($packages as $item)
                    <div class="col-md-4">
                        <div class="package_card">
                            <a href="{{route('home.details',$item->id)}}">
                               <div class="image">
                                    <img class="img-responsive" src="{{'storage/app/images/'.$item->image}}" alt="">
                               </div>
                                <h3>{{$item->title}} <span class="price pull-right">${{$item->price}}</span></h3>
                                <strong>{{$item->location}} {{$item->duration}}</strong>
                                <div class="details">{{$item->details}}</div>
                            </a>
                        </div>
                    </div>

                @endforeach

            </div>




            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="aligncenter"><h2 class="aligncenter">Our Services</h2>As Bangladesh is full of natural beauty and hidden treasure, its underdeveloped infrastructure can disrupt your travel plans. And this is why we are here to make your trip perfect with your needs. Our tour planning, time management, best accommodation, good quality food, local experience, freedom to blend in with this country and knowledgeable guides will impress you.
                        </div>
                        <br/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 info-blocks">
                        <i class="icon-info-blocks fa fa-bell-o"></i>
                        <div class="info-blocks-in">
                            <h3>Consulting</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur
                                aliquam, incidunt</p>
                        </div>
                    </div>
                    <div class="col-sm-4 info-blocks">
                        <i class="icon-info-blocks fa fa-hdd-o"></i>
                        <div class="info-blocks-in">
                            <h3>Strategy</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur
                                aliquam, incidunt</p>
                        </div>
                    </div>
                    <div class="col-sm-4 info-blocks">
                        <i class="icon-info-blocks fa fa-lightbulb-o"></i>
                        <div class="info-blocks-in">
                            <h3>Analysis</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur
                                aliquam, incidunt</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 info-blocks">
                        <i class="icon-info-blocks fa fa-code"></i>
                        <div class="info-blocks-in">
                            <h3>Investment</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur
                                aliquam, incidunt</p>
                        </div>
                    </div>
                    <div class="col-sm-4 info-blocks">
                        <i class="icon-info-blocks fa fa-compress"></i>
                        <div class="info-blocks-in">
                            <h3>Creative</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur
                                aliquam, incidunt</p>
                        </div>
                    </div>
                    <div class="col-sm-4 info-blocks">
                        <i class="icon-info-blocks fa fa-html5"></i>
                        <div class="info-blocks-in">
                            <h3>24/7 Support</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur
                                aliquam, incidunt</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aligncenter"><h2 class="aligncenter">Upcoming Packages</h2>
                    </div>
                    <br/>
                </div>
            </div>

{{--Upcoming --}}
            <div class="row box-section">
                @foreach($packages as $item)
                    <div class="col-md-4">
                        <div class="package_card upcoming">
                            <a href="{{route('home.details',$item->id)}}">
                                <div class="image">
                                    <img class="img-responsive" src="{{'storage/app/images/'.$item->image}}" alt="">
                                </div>
                               <div class="title">
                                    <h4>{{$item->title}} <span class="price pull-right">${{$item->price}}</span></h4>
                                <strong>{{$item->duration}}</strong>
                               </div>
                                <div class="schedule">
                                   @if($item->schedule->count() >=1)
                                        <ul>
                                            @foreach($item->schedule as $sche)

                                                <li>
                                                    {{$sche->start}} - {{$sche->end}} ({{$sche->status}})
                                                </li>

                                            @endforeach
                                        </ul>
                                       @else
                                       <h5 class="text-danger">
                                           No Schedule Added Yet
                                       </h5>
                                    @endif

                                </div>
                            </a>
                        </div>
                    </div>

                @endforeach

            </div>





        </div>

    </section>

    <section class="section-padding gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h2>We Provide the Best Tourism Packages</h2>
                        <p>We are fully experienced in any private and group tour in Bangladesh offer a solution for visiting Bangladesh to make your travel pleasant. We focus on meeting expectations & satisfaction of every guest visiting our country and try to help you organize your stay in the best way. </p>

                           <p> Our unique and custom tours are designed to give you the opportunity to see Bangladesh from unusual viewpoints. Through our website we try to supply you with as much information as we can to give you opportunity to check all possibilities before making any travel arrangements. We are continually developing the travel guide section so you can find here all the information you need before coming to Bangladesh.</p>

                        <p>Our aim is to make your stay in Bangladesh an unforgettable experience. We hope you will decide to ask us for help and we look forward to being at your service. We also invite to cooperation travel agencies from all over the world who wish to supply work with high-quality services in Bangladesh.</p>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="about-text">
                        <p>Our unique and custom tours are designed to give you the opportunity to see Bangladesh from unusual viewpoints. Through our website we try to supply you with as much information as we can to give you opportunity to check all possibilities before making any travel arrangements. We are continually developing the travel guide section so you can find here all the information you need before coming to Bangladesh.</p>

                        <ul class="withArrow">
                            <li><span class="fa fa-angle-right"></span> Lorem ipsum dolor sit amet</li>
                            <li><span class="fa fa-angle-right"></span> consectetur adipiscing elit</li>
                            <li><span class="fa fa-angle-right"></span> Curabitur aliquet quam id dui</li>
                            <li><span class="fa fa-angle-right"></span> Donec sollicitudin molestie malesuada.</li>
                        </ul>
                        <a href="#" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="about-image">
                        <img src="public/assets/img/about.png" alt="About Images">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="content-3-10" class="content-block data-section nopad content-3-10">
        <div class="image-container col-sm-6 col-xs-12 pull-left">
            <div class="background-image-holder">

            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6 col-xs-12 content">
                    <div class="editContent">
                        <h3>Why We’re Different</h3>
                    </div>
                    <div class="editContent">
                        <p>Our unique and custom tours are designed to give you the opportunity to see Bangladesh from unusual viewpoints. Through our website we try to supply you with as much information as we can to give you opportunity to check all possibilities before making any travel arrangements. We are continually developing the travel guide section so you can find here all the information you need before coming to Bangladesh.</p>

                    </div>
                    <a href="{{url('tour_packages')}}" class="btn btn-outline btn-outline outline-dark">Our Packages</a>
                </div>
            </div><!-- /.row-->
        </div><!-- /.container -->
    </section>




    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="widget">
                        <h5 class="widgetheading">Our Contact</h5>
                        <address>
                            <strong>Bootstrap company Inc</strong><br>
                            JC Main Road, Near Silnile tower<br>
                            Pin-21542 NewYork US.
                        </address>
                        <p>
                            <i class="icon-phone"></i> (123) 456-789 - 1255-12584 <br>
                            <i class="icon-envelope-alt"></i> email@domainname.com
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="#">Latest Events</a></li>
                            <li><a href="#">Terms and conditions</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="widget">
                        <h5 class="widgetheading">Latest posts</h5>
                        <ul class="link-list">
                            <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                            <li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
                            <li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="widget">
                        <h5 class="widgetheading">Recent News</h5>
                        <ul class="link-list">
                            <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                            <li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
                            <li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="copyright">
                            <p>
                                <span>&copy; Bootstrap Template 2018 All right reserved. Template By </span><a
                                    href="http://webthemez.com/free-bootstrap-templates/" target="_blank">WebThemez</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <ul class="social-network">
                            <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                            </li>
                            <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                            </li>
                            <li><a href="#" data-placement="top" title="Google plus"><i
                                        class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="public/assets/js/jquery.js"></script>
<script src="public/assets/js/jquery.easing.1.3.js"></script>
<script src="public/assets/js/bootstrap.min.js"></script>
<script src="public/assets/js/jquery.fancybox.pack.js"></script>
<script src="public/assets/js/jquery.fancybox-media.js"></script>
<script src="public/assets/js/jquery.flexslider.js"></script>
<script src="public/assets/js/animate.js"></script>
<!-- Vendor Scripts -->
<script src="public/assets/js/modernizr.custom.js"></script>
<script src="public/assets/js/jquery.isotope.min.js"></script>
<script src="public/assets/js/jquery.magnific-popup.min.js"></script>
<script src="public/assets/js/animate.js"></script>
<script src="public/assets/js/custom.js"></script>
<script src="public/assets/js/main.js"></script>
</body>
</html>
