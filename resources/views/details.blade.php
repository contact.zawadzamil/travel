@extends('editLayout')
@section('body_parts')
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Toure Details</h2>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	<section class="section-padding">
		<div class="container">
			<div class="row showcase-section">
				<div class="col-md-6">
					<div class="image">
                        <img src="{{'../storage/app/images/'.$package->image}}" alt="showcase image">
                    </div>
				</div>
				<div class="col-md-6">
					<div class="about-text">
						<h3>{{$package->title}}</h3>
						<p>{{$package->details}}</p>
                        <h5 class="mt-4"> Price (Per Person) : {{$package->price}}$</h5>

					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-padding gray-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="about-text">
{{--                        Heighlight --}}
                        <h5>Tour Highlights</h5>
						<p>{{$package->tour_highlights}}</p>

{{--                        Includes --}}

						<ul class="withArrow">
                            <h5>Includes</h5>
                            @foreach($includes as $item)
                                @if($item != '')
                                    <li><span class="fa fa-angle-right"></span>
                                        {{$item}}
                                    </li>
                                @endif


                            @endforeach


						</ul>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 ">
{{--                    Excludes --}}
                    <div class="exclude">
                        <ul class="withArrow">
                            <h5>Excludes</h5>
                            @foreach($excludes as $item)
                                @if($item != '')
                                    <li><span class="fa fa-angle-right"></span>
                                        {{$item}}
                                    </li>
                                @endif


                            @endforeach


                        </ul>
                    </div>
{{--                    Notes --}}
                    <div class="notes">
                        <h5>Notes</h5>
                        <p>
                            {{$package->notes}}
                        </p>
                    </div>

				</div>
			</div>
            <div class="row mt-4">
{{--                Attractions --}}
                <div class="col-md-6 col-sm-6">
                    <div class="attractions">
                        <h5>Attractions</h5>
                       <div class="attraction">
                           @foreach($titles as $item)
                               <button class="accordion">
                                   {{$item}}
                               </button>
                               <div class="panel">
                                   @foreach($attraction as $at)
                                       @if($at->title == $item)
                                           <p>{{$at->details}}</p>

                                       @endif.

                                   @endforeach

                               </div>

                           @endforeach

                       </div>
                    </div>

                </div>
{{--                Itinerary --}}
                <div class="col-md-6 col-sm-6">
                    <div class="itineraries">
                        <h5>Tour Itinerary</h5>
                        <div class="itinerary">
                            @foreach($itineraries as $item)
                                <button class="accordion">
                                    {{$item->day}} : {{$item->title}}
                                </button>
                                <div class="panel">
                                    <p>{{$item->details}}</p>
                                </div>

                            @endforeach


                        </div>

                    </div>
                </div>

            </div>

            <div class="row gallery">
{{--                Gallery --}}
                <h4>Gallery</h4>
                @if($package->gallery->count()==0)
                    <h5>No Gallery Added Yet!</h5>
                @else
                    @foreach($package->gallery as $item)

                        <div class="col-md-3 col-sm-4 ">
                            <div class="image">
                                <img src="{{asset('storage/app/images/'.$item->image)}}" alt="Not Found">
                            </div>
                        </div>
                    @endforeach
                @endif


            </div>
		</div>
	</section>
	</section>
    <script>

    </script>
	@endsection
