@extends('admin.layout')
@section('body_parts')
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- END SIDEBAR -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            @include('admin.sidebar')
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                @include('admin.page-header')
                <!-- END PAGE HEADER-->

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase"> Add User</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif
                                <form action="{{route('user.store')}}" id="myform"  method="POST"   class="form-horizontal">
                                    @csrf
                                    <div class="form-body">
                                        {{--                                     User  Name --}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Name</label>
                                           <div class="col-md-6">
                                               <input type="text" name="name" class="form-control" placeholder="Enter Name" required="">
                                           </div>

                                        </div>





                                        {{--                                        Email --}}

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Email</label>
                                            <div class="col-md-6">
                                                <input type="email" name="email" class="form-control" placeholder="Enter Email" required="">
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Password</label>
                                            <div class="col-md-6">
                                                <input type="password" name="password" class="form-control" placeholder="Enter Password" required="">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Role</label>
                                            <div class="col-md-6">
                                                <select name="role" class="form-control">
                                                    <option value="" selected disabled>Select a Role</option>
                                                    @foreach($roles as $item)
                                                        <option style="text-transform: capitalize"  value="{{$item -> id}}">{{$item->name}}</option>

                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>



                                        {{--                                        Submit Button--}}
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class=" submit_button_div">
                                                    <button type="submit" class="btn green " id="send">Submit</button>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->


                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

    </div>
    <script src="http://code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="public/admin/assets/customJS/gallery.js" type="text/javascript"></script>
    <script src="public/admin/assets/customJS/schedule.js" type="text/javascript"></script>








@endsection
