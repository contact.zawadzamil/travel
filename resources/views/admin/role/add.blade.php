@extends('admin.layout')
@section('body_parts')
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- END SIDEBAR -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            @include('admin.sidebar')
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                @include('admin.page-header')
                <!-- END PAGE HEADER-->

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase"> Add Role</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif
                                <form action="{{route('role.store')}}" id="myform"  method="POST"   class="form-horizontal">
                                    @csrf
                                    <div class="form-body">
                                        {{--                                     Type  Name --}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Role</label>
                                            <div class="col-md-6">
                                                <select class="form-control"  name="role" required>
                                                    <option value="" disabled selected>Select a Role</option>
                                                    <option value="administrator">Administrator</option>
                                                    <option value="admin">Admin</option>
                                                    <option value="user">User</option>
                                                    <option value="editor">Editor</option>
                                                    <option value="support">Support</option>

                                                </select>

                                            </div>
                                        </div>





                                        {{--                                        Permission --}}

                                        <div class="form-group permission">
                                            <label class="col-md-3 control-label">Permissions</label>
                                            <div class="col-md-9">
                                                <div class="mt-checkbox-list">
                                                    @foreach($permissions as $item)
                                                        <div class="col-md-6">
                                                            <label class="mt-checkbox">
                                                                <input type="checkbox" name="permission[]" value="{{$item ->id}}"> {{$item->name}}
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    @endforeach



                                                </div>
                                            </div>
                                        </div>

                                        {{--                                        Submit Button--}}
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class=" submit_button_div">
                                                    <button type="submit" class="btn green " id="send">Submit</button>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->


                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

    </div>
    <script src="http://code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="public/admin/assets/customJS/gallery.js" type="text/javascript"></script>
    <script src="public/admin/assets/customJS/schedule.js" type="text/javascript"></script>








@endsection
