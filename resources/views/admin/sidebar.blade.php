
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- END SIDEBAR -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->

            <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="nav-item start active open">
                    <a href="{{url('admin')}}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>

                </li>

                <li class="nav-item  ">
{{--                    <a href="javascript:;" class="nav-link nav-toggle">--}}
                        <a href="{{url('packages')}}" class="nav-link">
                        <i class="icon-puzzle"></i>
                        <span class="title">Packages</span>
                        <span class="arrow"></span>
                    </a>
{{--                    <ul class="sub-menu">--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_date_time_pickers.html" class="nav-link ">--}}
{{--                                <span class="title">Date & Time Pickers</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_color_pickers.html" class="nav-link ">--}}
{{--                                <span class="title">Color Pickers</span>--}}
{{--                                <span class="badge badge-danger">2</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_select2.html" class="nav-link ">--}}
{{--                                <span class="title">Select2 Dropdowns</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_bootstrap_multiselect_dropdown.html" class="nav-link ">--}}
{{--                                <span class="title">Bootstrap Multiselect Dropdowns</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_bootstrap_select.html" class="nav-link ">--}}
{{--                                <span class="title">Bootstrap Select</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_multi_select.html" class="nav-link ">--}}
{{--                                <span class="title">Bootstrap Multiple Select</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_bootstrap_select_splitter.html" class="nav-link ">--}}
{{--                                <span class="title">Select Splitter</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_clipboard.html" class="nav-link ">--}}
{{--                                <span class="title">Clipboard</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_typeahead.html" class="nav-link ">--}}
{{--                                <span class="title">Typeahead Autocomplete</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_bootstrap_tagsinput.html" class="nav-link ">--}}
{{--                                <span class="title">Bootstrap Tagsinput</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_bootstrap_switch.html" class="nav-link ">--}}
{{--                                <span class="title">Bootstrap Switch</span>--}}
{{--                                <span class="badge badge-success">6</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_bootstrap_maxlength.html" class="nav-link ">--}}
{{--                                <span class="title">Bootstrap Maxlength</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_bootstrap_fileinput.html" class="nav-link ">--}}
{{--                                <span class="title">Bootstrap File Input</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_bootstrap_touchspin.html" class="nav-link ">--}}
{{--                                <span class="title">Bootstrap Touchspin</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_form_tools.html" class="nav-link ">--}}
{{--                                <span class="title">Form Widgets & Tools</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_context_menu.html" class="nav-link ">--}}
{{--                                <span class="title">Context Menu</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_editors.html" class="nav-link ">--}}
{{--                                <span class="title">Markdown & WYSIWYG Editors</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_code_editors.html" class="nav-link ">--}}
{{--                                <span class="title">Code Editors</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_ion_sliders.html" class="nav-link ">--}}
{{--                                <span class="title">Ion Range Sliders</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_noui_sliders.html" class="nav-link ">--}}
{{--                                <span class="title">NoUI Range Sliders</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item  ">--}}
{{--                            <a href="components_knob_dials.html" class="nav-link ">--}}
{{--                                <span class="title">Knob Circle Dials</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
                </li>
{{--                Types --}}
                <li class="nav-item  ">
                    {{--                    <a href="javascript:;" class="nav-link nav-toggle">--}}
                    <a href="{{route('type.index-web')}}" class="nav-link">
                        <i class="fa fa-database"></i>
                        <span class="title">Types</span>
                        <span class="arrow"></span>
                    </a>
                </li>

{{--                Gallery --}}
                <li class="nav-item  ">
                    {{--                    <a href="javascript:;" class="nav-link nav-toggle">--}}
                    <a href="{{route('gallery.index')}}" class="nav-link">
                        <i class="fa fa-picture-o"></i>
                        <span class="title">Gallery</span>
                        <span class="arrow"></span>
                    </a>
                </li>

{{--                Schedule --}}

                <li class="nav-item  ">
                    {{--                    <a href="javascript:;" class="nav-link nav-toggle">--}}
                    <a href="{{route('schedule.index')}}" class="nav-link">
                        <i class="fa fa-calendar-check-o"></i>
                        <span class="title">Schedule</span>
                        <span class="arrow"></span>
                    </a>
                </li>

{{--                Message--}}
                <li class="nav-item  ">
                    {{--                    <a href="javascript:;" class="nav-link nav-toggle">--}}
                    <a href="{{route('message.index')}}" class="nav-link">
                        <i class="fa fa-comments-o"></i>
                        <span class="title">Messages</span>
                        <span class="arrow"></span>
                    </a>
                </li>


{{--                Roles --}}
                @hasanyrole('administrator|admin')
                <li class="nav-item  ">

                    <a href="{{route('role.index')}}" class="nav-link">
                        <i class="fa fa-users"></i>
                        <span class="title">Roles</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                @endrole

{{--                Users--}}
                @hasanyrole('administrator')
                <li class="nav-item  ">

                    <a href="{{route('push-index')}}" class="nav-link">
                        <i class="fa fa-user-plus"></i>
                        <span class="title">Users</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                @endrole
{{--                // Pusher --}}
                <li class="nav-item  ">

                    <a href="{{route('push-index')}}" class="nav-link">
                        <i class="fa fa-user-plus"></i>
                        <span class="title">Push Notification</span>
                        <span class="arrow"></span>
                    </a>
                </li>
            </ul>


            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->

