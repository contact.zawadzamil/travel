@extends('admin.editLayout')
@section('body_parts')
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- END SIDEBAR -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            @include('admin.sidebar')
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                @include('admin.page-header')
                <!-- END PAGE HEADER-->

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase"> Add Schedule</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif
                                <form action="{{route('schedule.update',$schedule->id)}}" id="myform"  method="POST"   class="form-horizontal">
                                    @csrf
                                    <div class="form-body">
                                        {{--                                     Type  Name --}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Package</label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="PackageSelectSc" name="package"  required>
                                                    <option value="{{$schedule->package->id}}"  selected>{{$schedule->package->title}}</option>
                                                    <option value="" disabled >Select a Package</option>

                                                </select>

                                            </div>
                                        </div>

                                        {{--                                        Duration Placeholder --}}
                                        <div class="form-group ">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6 duration_div">
                                                <h4>Duration : <span id="duration">
                                                    </span></h4>
                                            </div>
                                        </div>

                                        {{--                                        Dates --}}
                                        <div class="form-group">
                                            {{--                                            Start Date --}}
                                            <label class="col-md-3 control-label">Start Date</label>
                                            <div class="col-md-3">
                                                <input id="startDate" name="start" class="form-control" type="date" value="{{$schedule->start}}" required />
                                            </div>

                                            {{--                                            End Date  --}}
                                            <label class="col-md-1 control-label">End Date</label>
                                            <div class="col-md-3">
                                                <input id="endDate" name="end" class="form-control" type="date" value="{{$schedule->end}}" required />
                                            </div>

                                        </div>

                                        {{--                                        Status --}}

                                        <div class="form-group status">
                                            <label class="col-md-3 control-label">Select Status</label>
                                            <div class="col-md-8">
                                                <div class="mt-radio-list">
                                                    <label class="mt-radio">
                                                        <input type="radio" name="status" class="status" id="available" value="available to book" checked=""> Available to Book
                                                        <span></span>
                                                    </label>
                                                    <label class="mt-radio">
                                                        <input type="radio" name="status"  class="status" id="not_available" value="unavailable" > Not Available
                                                        <span></span>
                                                    </label>

                                                    <label class="mt-radio">
                                                        <input type="radio" name="status" class="status" id="booked" value="full booked" > Full Booked
                                                        <span></span>
                                                    </label>



                                                </div>
                                            </div>
                                        </div>
{{--                                        Hidden Input --}}

                                        <input type="hidden" id="response_status_sc" value="{{$schedule->status}}">
                                        {{--                                        Submit Button--}}
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class=" submit_button_div">
                                                    <button type="submit" class="btn green " id="send">Submit</button>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->


                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

    </div>
    <script src="http://code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../public/admin/assets/customJS/gallery.js" type="text/javascript"></script>
    <script src="../public/admin/assets/customJS/schedule.js" type="text/javascript"></script>








@endsection
