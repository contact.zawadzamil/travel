@extends('admin.layout')
@section('body_parts')
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- END SIDEBAR -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            @include('admin.sidebar')
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
              @include('admin.page-header')
                <!-- END PAGE HEADER-->

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase"> Create Gallery</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif
                                    @if(session()->has('failed'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('failed') }}
                                        </div>
                                    @endif
                                <form method="POST" action="{{route('gallery.store')}}"   class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-body">

                                        <div class="form-group">
                                            {{--                                            Image --}}
                                            <label for="exampleInputFile" class="col-md-3 control-label">Image</label>
                                            <div class="col-md-6">
                                                <input type="file" id="image" name="image"   required="">
                                                <p class="help-block"> Choose Package Image </p>
                                                <div class="imagePreview" id="imagePreview">

                                                    <img id="preview_image"  >
                                                    <div class="button">
                                                        <a class="btn btn-danger" id="cross_image">X</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
{{--                                        Package --}}
                                        <div class="form-group">
                                            <label for="exampleInputFile" class="col-md-3 control-label">Packages</label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="PackageSelect" name="package" required>
                                                    <option value="" disabled selected>Select a Package</option>

                                                </select>
                                            </div>

                                        </div>


                                        {{--                                        Submit Button--}}
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class=" submit_button_div">
                                                    <button type="submit" class="btn green " id="submit">Upload</button>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>


                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->


                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

    </div>


    {{--JQUERY --}}
    <script src="http://code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>

    <script src="public/admin/assets/customJS/gallery.js" type="text/javascript"></script>













@endsection
