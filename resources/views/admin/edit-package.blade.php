@extends('admin.editLayout')
@section('body_parts')
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- END SIDEBAR -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            @include('admin.sidebar')
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN THEME PANEL -->
                <div class="theme-panel">
                    <div class="toggler tooltips" data-container="body" data-placement="left" data-html="true"
                         data-original-title="Click to open advance theme customizer panel">
                        <i class="icon-settings"></i>
                    </div>
                    <div class="toggler-close">
                        <i class="icon-close"></i>
                    </div>
                    <div class="theme-options">
                        <div class="theme-option theme-colors clearfix">
                            <span> THEME COLOR </span>
                            <ul>
                                <li class="color-default current tooltips" data-style="default" data-container="body"
                                    data-original-title="Default"></li>
                                <li class="color-grey tooltips" data-style="grey" data-container="body"
                                    data-original-title="Grey"></li>
                                <li class="color-blue tooltips" data-style="blue" data-container="body"
                                    data-original-title="Blue"></li>
                                <li class="color-dark tooltips" data-style="dark" data-container="body"
                                    data-original-title="Dark"></li>
                                <li class="color-light tooltips" data-style="light" data-container="body"
                                    data-original-title="Light"></li>
                            </ul>
                        </div>
                        <div class="theme-option">
                            <span> Theme Style </span>
                            <select class="layout-style-option form-control input-small">
                                <option value="square" selected="selected">Square corners</option>
                                <option value="rounded">Rounded corners</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span> Layout </span>
                            <select class="layout-option form-control input-small">
                                <option value="fluid" selected="selected">Fluid</option>
                                <option value="boxed">Boxed</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span> Header </span>
                            <select class="page-header-option form-control input-small">
                                <option value="fixed" selected="selected">Fixed</option>
                                <option value="default">Default</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span> Top Dropdown</span>
                            <select class="page-header-top-dropdown-style-option form-control input-small">
                                <option value="light" selected="selected">Light</option>
                                <option value="dark">Dark</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span> Sidebar Mode</span>
                            <select class="sidebar-option form-control input-small">
                                <option value="fixed">Fixed</option>
                                <option value="default" selected="selected">Default</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span> Sidebar Style</span>
                            <select class="sidebar-style-option form-control input-small">
                                <option value="default" selected="selected">Default</option>
                                <option value="compact">Compact</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span> Sidebar Menu </span>
                            <select class="sidebar-menu-option form-control input-small">
                                <option value="accordion" selected="selected">Accordion</option>
                                <option value="hover">Hover</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span> Sidebar Position </span>
                            <select class="sidebar-pos-option form-control input-small">
                                <option value="left" selected="selected">Left</option>
                                <option value="right">Right</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span> Footer </span>
                            <select class="page-footer-option form-control input-small">
                                <option value="fixed">Fixed</option>
                                <option value="default" selected="selected">Default</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- END THEME PANEL -->
                <h1 class="page-title"> Rowreorder Extension
                    <small>rowreorder extension demos</small>
                </h1>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Tables</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <span>Datatables</span>
                        </li>
                    </ul>
                    <div class="page-toolbar">
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle"
                                    data-toggle="dropdown" data-hover="dropdown" data-delay="1000"
                                    data-close-others="true"> Actions
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>
                                    <a href="#">
                                        <i class="icon-bell"></i> Action</a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-shield"></i> Another action</a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i> Something else here</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#">
                                        <i class="icon-bag"></i> Separated link</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- END PAGE HEADER-->

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase"> Edit Package</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif
                                <form action="{{route('package.update',$package->id)}}" method="POST" enctype="multipart/form-data"
                                      class="form-horizontal">
                                    @csrf
                                    <div class="form-body">
                                        {{--                                        Title --}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Title</label>
                                            <div class="col-md-6">
                                                <input type="text" name="title" class="form-control"
                                                       placeholder="Enter Title" required="" value="{{$package->title}}">

                                            </div>
                                        </div>
                                        {{--                                        Image & Type--}}
                                        <div class="form-group ">
                                            {{--                                            Image --}}
                                            <label for="exampleInputFile" class="col-md-3 control-label">Image</label>
                                            <div class="col-md-2">
                                                <input type="file" id="image" name="image" >

                                                <p class="help-block"> Choose Package Image </p>
                                                <div class="imagePreviewEdit" id="imagePreview">

                                                    <img id="preview_image" src="{{'../storage/app/images/'.$package->image}}" alt="Not Found">
                                                    <div class="button">
                                                        <a class="btn btn-danger" id="cross_image">X</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {{--                                                Type --}}
                                                <label class="col-md-2 control-label">Type</label>

                                                <div class="col-md-6">
                                                    <select class="form-control" id="typeSelect" name="type" required>
                                                        <option value="{{$package->type_id}}" selected >{{$package->Name}}</option>


                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        {{--                                        Duration--}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Duration</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" name="duration"
                                                       placeholder="Enter Duration" value="{{$package->duration}}" required>
                                                <span class="help-block"> ex. 4 Nights 5 Days  </span>

                                            </div>
                                            <label class="col-md-1 control-label">Price</label>
                                            <div class="col-md-2">
                                                <input type="number" class="form-control" name="price"
                                                       placeholder="Enter Price Per Person ($)" min="1" value="{{$package->price}}" required>
                                                <span class="help-block"> ex. 3600  </span>

                                            </div>
                                        </div>

                                        {{--                                        Details --}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Details</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" name="details" rows="3"  required=""
                                                          placeholder="Enter Details">
                                                    {{$package->details}}
                                                </textarea>
                                            </div>
                                        </div>

                                        {{--                                        Location --}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Locations</label>
                                            <div class="col-md-6">
                                                <input type="text" name="location" required class="form-control"
                                                       placeholder="Enter Location" value="{{$package->location}}">
                                                <span class="help-block"> ex. Dhaka, Bagerhat, Sundarbans,  </span>
                                            </div>
                                        </div>
                                        {{--                                        Attraction--}}
                                        <div class="form-group attraction">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6  text-center">
                                                <h5 class="text-center d-block">Attraction</h5>
                                                <label for="" class="control-label">Title</label>
                                                <input type="text" id="attraction_title"
                                                       class="form-control input-inline input-medium"
                                                       placeholder="Enter Attraction Title" name="attraction_title">

                                                <div class="details">
                                                    <label class="control-label ">Details</label>
                                                    <textarea class="form-control" id="attraction_details"
                                                              name="attraction_details" rows="3"></textarea>
                                                </div>
                                                <button class="btn btn-primary " id="add_attraction" type="button ">
                                                    Add
                                                </button>


                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="attraction_added">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th> #</th>
                                                            <th>Title</th>
                                                            <th> Details</th>
                                                            <th>Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                        {{--                                        Tour Availabale & Best Time to Go --}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tour Available</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control input-inline input-medium" value="{{$package->tour_available}}"
                                                       placeholder="Enter Tour Available" name="tour_available">

                                            </div>
                                            <label class="col-md-2 control-label">Best time to go:</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control input-inline input-medium" value="{{$package->best_time}}"
                                                       placeholder="Enter Best time to go:" name="best_time">

                                            </div>

                                        </div>

                                        {{--                                        Tour Highlight --}}

                                        <div class="form-group tour_highlight">
                                            <label class="col-md-3 control-label">Tour Highlights</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" name="tour_highlight" id="tour_highlight"
                                                          rows="3" required=""
                                                          placeholder="Enter Tour Highlight">
                                                    {{$package->tour_highlights}}
                                                </textarea>
                                                <div class="button">

                                                </div>
                                            </div>

                                        </div>


                                        {{--                                        Tour Itinerary --}}
                                        <div class="form-group tour_itinerary">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6  text-center">
                                                <h5 class="text-center d-block">Tour Itinerary</h5>
                                                <label for="" class="control-label">Day</label>
                                                <input type="text" id="tour_itinerary_day"
                                                       class="form-control input-inline input-medium"
                                                       placeholder="Enter Itinerary Day" name="tour_itinerary_day">

                                                <label for="" class="control-label">Title</label>
                                                <input type="text" id="tour_itinerary_title"
                                                       class="form-control input-inline input-medium"
                                                       placeholder="Enter Itinerary Title" name="tour_itinerary_title">


                                                <div class="details">
                                                    <label class="control-label ">Details</label>
                                                    <textarea class="form-control" id="tour_itinerary_details"
                                                              placeholder="Enter Itinerary Details"
                                                              name="tour_itinerary_details" rows="4"></textarea>
                                                </div>
                                                <button class="btn btn-primary " id="add_itinerary" type="button ">Add
                                                    Itinerary
                                                </button>


                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="tour_itinerary_added attraction_added">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th> #</th>
                                                            <th>Day</th>
                                                            <th>Title</th>
                                                            <th> Details</th>
                                                            <th>Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody id="itinerary_tbody">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>

                                        {{--                                        Includes                                       --}}
                                        <div class="form-group includes tour_highlight">
                                            <label class="col-md-3 control-label">Includes</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" name="includes" rows="3" id="includes"
                                                          required placeholder="Enter Include">
                                                    {{$package->includes}}
                                                </textarea>
                                                <div class="button">

                                                </div>
                                            </div>

                                        </div>


                                        {{--                                        Excludes--}}
                                        <div class="form-group excludes tour_highlight ">
                                            <label class="col-md-3 control-label">Excludes</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" name="excludes" id="excludes" required
                                                          placeholder="Enter Exclude">
                                                    {{$package->excludes}}
                                                </textarea>
                                                <div class="button">

                                                </div>
                                            </div>

                                        </div>


                                        {{--                                        Notes --}}

                                        <div class="form-group notes  ">
                                            <label class="col-md-3 control-label">Notes</label>
                                            <div class="col-md-6">
                                                <textarea class="form-control" name="notes" id="notes" rows="5" required
                                                          placeholder="Enter Notes" maxlength="5000">
                                                    {{$package->notes}}

                                                </textarea>

                                            </div>

                                        </div>
                                        {{--                                        Status--}}
                                        <div class="form-group status">
                                            <label class="col-md-4 control-label">Select Status</label>
                                            <div class="col-md-8">
                                                <div class="mt-radio-list">
                                                    <label class="mt-radio">
                                                        <input type="radio" class="status" name="status" id="available"
                                                               value="available" > Available
                                                        <span></span>
                                                    </label>
                                                    <label class="mt-radio">
                                                        <input type="radio" class="status" name="status" id="unavailable"
                                                               value="unavailable"> Not Available
                                                        <span></span>
                                                    </label>

                                                    <label class="mt-radio">
                                                        <input type="radio" class="status" name="status" id="booked" value="booked">
                                                        Booked
                                                        <span></span>
                                                    </label>

                                                    <label class="mt-radio">
                                                        <input type="radio" class="status" name="status" id="upcoming"
                                                               value="upcoming"> Upcoming
                                                        <span></span>
                                                    </label>

                                                </div>
                                            </div>
                                        </div>

{{--                                        Hidden Inputs for Data passing --}}
                                        <input type="hidden" id="attraction_array" name="attraction_array[]" value="{{$attractions}}">

                                        <input type="hidden" id="itinerary_array" name="itinerary_array[]" value="{{$itis}}">

                                        <input type="hidden" id="response_status" name="response_status" value="{{$package->status}}">

                                        {{--                                        Submit Button--}}
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class=" submit_button_div">
                                                    <button type="submit" class="btn green " id="submit">Submit</button>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->


                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

    </div>
    <script src="http://code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script>


@endsection
