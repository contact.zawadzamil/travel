@extends('layout')
@section('body_parts')
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="pageTitle">Home >> Meet Bangladesh</h3>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <section class="section-padding">
            <div class="container">
                <div class="row showcase-section">
                    <div class="col-md-6">
                        <img src="{{asset('storage/images/img6.jpg')}}" alt="showcase image">
                    </div>
                    <div class="col-md-6">
                        <div class="about-text">
                            <h3>Meet Bangladesh</h3>
                            <p>Bangladesh is a South-Asia greenest bijou- a land plaited with rivers & smiling peoples and exhilarating mix with fascinating history, vibrant cultures, panoramic beauties, historical ruins, flora and fauna, hills and forests, sandy sea beaches and wildlife, is waiting to welcome . The world’s longest sandy beach- Cox’s Bazaar, the world’s single largest mangrove forest- Sundarbans forest-the home of man eating tigers, Oceanic Kuakata, Coral island-Saint martin’s, the home of colorful Indigenous group Chittagong Hill Tracts Bandarban and Lake City Rangamati, the scenic beauty of the hilly regions, historic and archaeological sites etc. are life time experience for travelers. Bangladesh, a land of tourists, researchers, and wildlife and nature lovers.</p>

                        </div>
                    </div>
                </div>

{{--                // 4 Image Section --}}
                <div class="row image-section">
                    <div class="col-md-3">
                        <div class="image">
                            <img src="{{asset('storage/images/tiger.jpg')}}" alt="Tigher">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="image">
                            <img src="{{asset('storage/images/img2.jpg')}}" alt="Tigher">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="image">
                            <img src="{{asset('storage/images/img3.jpg')}}" alt="Tigher">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="image">
                            <img src="{{asset('storage/images/img4.jpg')}}" alt="Tigher">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">

         <div class="row description">
             <h4>A Land of Rivers</h4>
             <p>Bangladesh is a country braided together by more than 700 rivers, creating a green landscape that will surprise you. This is one of the world’s most densely populated countries, but its charm will surpass the turbulence. Whatever you have imagined about the country, if you go through the river, your perspective will let you pointed to a graceful experience. Not only the beauty of waves and the tides, the lives surrounding the river will fill your empty mind and help you to be merged with people. The lifestyle, Fishing techniques, small bazaar beside river, making small boats, cultivation on the bank, Festivals and cultural events and the smiling faces of riverside people will never disappoint you. So if you want to know the riverain Bangladesh, get a plan to jump to the wave. Boat small or big will help you to explore the country with the same pleasure.</p>
             <div class="image">

                 <img src="{{'storage/images/river.jpg'}}" alt="">
             </div>

         </div>
            <div class="row description">
                <h4>Hidden Riches</h4>
                <p>
                    The hidden riches of Bangladesh will seduce you gradually to be amazed like seasonal colors and smiling people. The world’s largest mangrove forest is here, the land of man-eater tigers, saltwater crocodiles, dotted deer, birds, bull sharks, various primates and snakes with more than 300 rivers named Sundarbans which covers an area of 10000 square kilometers. It’s been marked as UNESCO world heritage site along with Paharpur Buddist Temple and famous 15th Century creation ‘Sixty Domed Mosque’ in Bagerhat, will force you to call ‘Bangladesh’ as a beautiful country. Besides, the mosques and temples made more than 100 centuries, and the archeological monuments will enrich the historical tradition of this land to you.  Did you know that? This small country having the largest sea beach in the world. Cox’s Bazaar, the 120 km longest beach of Bay of Bengal, which can give you tranquility. And never miss Saint Martin Island, the island of blue water and coconut garden. Although, majority of the people are Muslim, you can find the separate world when you step down to the Chittagong hill tracts area. There is still home to Christian and Buddhist Adivasi tribal peoples, who spend life with their own cultures. Sylhet is familiar as ‘land of tea’ will give you the various taste of tea as well as the technique to sink greenery.   </p>


            </div>
            <div class="row description">
                <h4>Welcoming People</h4>
                <p>
                    Bangladeshi culture is famously welcoming and hospitable. If you enjoy making friends, blending with so people and traveling without any disturbance, this is apparently the best country to explore.  You could be served by being the center of attention very easily here, with full of joy and smiling faces.  </p>
                <div class=" row">
                    <div class="col-md-2">
                        <div class="little_image">
                            <img src="{{'storage/images/lt1.jpg'}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="little_image">
                            <img src="{{'storage/images/lt2.jpg'}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="little_image">
                            <img src="{{'storage/images/lt3.jpg'}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="little_image">
                            <img src="{{'storage/images/lt4.jpg'}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="little_image">
                            <img src="{{'storage/images/lt5.png'}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="little_image">
                            <img src="{{'storage/images/lt6.jpg'}}" alt="">
                        </div>
                    </div>
                </div>


            </div>
            <div class="row quick_facts">
                <h4 id="quick">Quick Facts</h4>
                <div class="list">
                    <ul>
                        <li>
                            <strong>Official Name: </strong> The People’s Republic of Bangladesh.
                        </li>
                        <li>
                            <strong>State Language: </strong> Bengali
                        </li>
                        <li>
                            <strong>Capital: </strong> Dhaka
                        </li>
                        <li>
                            <strong>Nationality: </strong> Bangladeshi
                        </li>
                        <li>
                            <strong>Name of Currency: </strong> Taka (TK)
                        </li>
                        <li>
                            <strong>Area: </strong> 56977 sq. miles or 147570 sq. km.
                        </li>
                        <li>
                            <strong>Territorial Water: </strong> 200 nautical miles.
                        </li>
                        <li>
                            <strong>Population: </strong> 2016 estimate- 162,951,560, 2011 census-149,772,364, Density- 1,106/ sq. km. (2,864.5/sq mi)
                        </li>
                        <li>
                            <strong>State Religion: </strong> Islam but other main religions namely Hinduism, Buddhism, Christianity are practiced in peace and harmony.
                        </li>
                        <li>
                            <strong>National Anthem: </strong>  The first ten lines of ‘Amar Sonar Bangla’ written by Nobel Luareate Rabindranath Tagore.
                        </li>
                        <li>
                            <strong>Geographical Location:   </strong>   North, West & South-India, East- India and Myanmar.
                        </li>
                        <li>
                            <strong>Drives: </strong>   On the left.
                        </li>
                        <li>
                            <strong>Time Zone: </strong> UTC +6 (BST)
                        </li>
                        <li>
                            <strong>Calling Code: </strong> +880
                        </li>
                        <li>
                            <strong>SO 3166 Code: </strong>BD
                        </li>
                    </ul>
                </div>
                <div class="image_info row">
                    <div class="col">
                        <div class="image">
                            <img src="{{asset('storage/images/n1.png')}}" alt="">
                            <div class="caption">
                                National Flag
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="image">
                            <img src="{{asset('storage/images/n2.png')}}" alt="">
                            <div class="caption">
                                Government Seal
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="image">
                            <img src="{{asset('storage/images/n3.png')}}" alt="">
                            <div class="caption">
                                National Emblem
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="image">
                            <img src="{{asset('storage/images/n4.png')}}" alt="">
                            <div class="caption">
                                Location Map
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="image">
                            <img src="{{asset('storage/images/n5.png')}}" alt="">
                            <div class="caption">
                                Orthographic Projection
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="container-fluid">
        <div class=" history_link">
            <h3><a href="{{url('history')}}">History Of Bangladesh</a> </h3>
        </div>
    </section>
@endsection
