@extends('layout')
@section('body_parts')
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="pageTitle">Home >> Language, Culture & Religion of Bangladesh</h3>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <section class="section-padding">
            <div class="container">
                <div class="container">
                    <div class="row lccr">
                        <h3>Language, Culture & Religion of Bangladesh</h3>
                        <div class="language inner_section">
                            <h4 class="sub_title">Language</h4>
                            <p>
                                The official language of Bangladesh is Bangla (or, Bengali). Dated even before the birth of Christ, Bangla was widely spoken in this region, and now spoken by more than 200 million people all over the world. It has a various dialects with different accents, pronunciations and minor grammatical changes in different region in Bangladesh. Based on usage, Bangla is divided in two forms: sadhu bhasha (formal language) and cholito bhasha (common language). If opting for second language, most Bangladeshi people chose English as they have practical efficiency in English and its interactive usage in common situation. Apart from our official language, there are a significant number of tribal languages are spoken by the tribes of Bangladesh. The major and well-known tribal languages are Garo, Khashia, Magh, Manipuri, Munda, Chakma, Tipra etc.
                            </p>
                        </div>
                        <div class="culture inner_section">
                            <h4 class="sub_title">Culture</h4>
                            <p>
                                Bangladesh has a rich and diverse culture which is reflected in the architecture, culture, literature, music, painting, clothing, discourse etc. Even in this era of rock ‘n’ roll, Bangladeshi culture is still being cherished and respected by the people of all over the world. Even the culture of the tribes is rich and diverse too. Clothing, one of the most important aspect of the culture of Bangladesh, defines the Bangladeshi people very skillfully. Saree — a finely embroidered cloth worn by the Bangladeshi woman and made by the skilled hands of Bangladeshi artisans — is now a global fashion trend. Festivals, held throughout the year with great zeal and zest, play a significant role in our culture. Some festivals are so intensely rooted in our cultural and social base that they are still being continued after centuries. Some festivals are based on social and political significances with marking our communal and national value, some are religious, and some are even observed seasonally. Bangladeshi people have so many reasons to cheer their life and soul. This is why this land is called the Land of Festivals.
                            </p>
                        </div>
                        <div class="religion inner_section">
                            <h4 class="sub_title">Religion</h4>
                            <p>
                                Bangladesh has a rich and diverse culture which is reflected in the architecture, culture, literature, music, painting, clothing, discourse etc. Even in this era of rock ‘n’ roll, Bangladeshi culture is still being cherished and respected by the people of all over the world. Even the culture of the tribes is rich and diverse too. Clothing, one of the most important aspect of the culture of Bangladesh, defines the Bangladeshi people very skillfully. Saree — a finely embroidered cloth worn by the Bangladeshi woman and made by the skilled hands of Bangladeshi artisans — is now a global fashion trend. Festivals, held throughout the year with great zeal and zest, play a significant role in our culture. Some festivals are so intensely rooted in our cultural and social base that they are still being continued after centuries. Some festivals are based on social and political significances with marking our communal and national value, some are religious, and some are even observed seasonally. Bangladeshi people have so many reasons to cheer their life and soul. This is why this land is called the Land of Festivals.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </section>
    <section class="container-fluid">
        <div class=" history_link">
            <h3><a href="{{url('language')}}">Geography and Climate of Bangladesh</a> </h3>
        </div>
    </section>
@endsection
