@extends('layout')
@section('body_parts')
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="pageTitle">Home >> History of Bangladesh</h3>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <section class="section-padding">
            <div class="container">
              <div class="container">
                  <div class="row history">
                      <h3>History of Bangladesh</h3>
                      <p>Bangladesh officially known as the People’s Republic of Bangladesh is an independent nation located in South Asia. It makes up the eastern and largest portion of the ethnolinguistic region of Bengal. It is situated at the zenith of the Bay of Bengal and is bordered by Myanmar and India, and separated from Bhutan and Nepal by the thin Siliguri Corridor. Present-day Bangladesh came out as a sovereign country in 1971 after breaking away and gaining independence from Pakistan in the Bangladesh liberation war. Its early history was characterized by internal fighting, a succession of Indian empires, and a scuffle between Buddhism and Hinduism for dominance. The borders of modern Bangladesh were formed after the partition of India and Bengal on August 1947, when the area became East Pakistan as a section of the newly established State of Pakistan following the Radcliff Line. The name Bangladesh was initially written as two words, Bangla Desh. Bangla is a keyword for both the Bengali language and the Bengal region. The exact origin of the term is, however, not known. Below, some of the key events in the history of Bangladesh have been explained in brief.  </p>
                  </div>
                  <div class="row independence" >
                      <h4 class="text-gray">Bangladesh War of independence</h4>
                      <p>
                          On 23 March 1971, the Bangladeshi flag was raised for the first time. Operation Searchlight was, however, launched on 26 March 1971 by the Pakistani military junta who massacred Bengali politicians, students, intellectuals, military defectors, and civil servants during the 1971 Bangladesh genocide.
                      </p>
                      <p>During the liberation war, Bengali locals proclaimed a declaration of independence and created the Mukti Bahini, the Bangladeshi National Liberation Army. During the war, the army held Bengali’s countryside and waged guerilla operations against the Pakistani forces. India offered support to the army during the war. The United States and the Soviet Union also sent naval forces to the Bengal Bay to offer support. The war lasted for nine months and it came to an end when the Pakistani military surrendered to the Bangladesh-India Allied Forces on 16th December 1971.</p>
                      <div class="row image-section">
                          <div class="col-md-4">
                              <div class="image">
                                  <img src="{{asset('storage/images/History1.jpg')}}" alt="">
                                  <div class="caption">
                                      <p>Signing of Independence</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="image">
                                  <img src="{{asset('storage/images/History5.jpg')}}" alt="">
                                  <div class="caption">
                                      <p>Refuges 1971</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="image">
                                  <img src="{{asset('storage/images/History6.jpg')}}" alt="">
                                  <div class="caption">
                                      <p>Refuges 1971</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row detailed">
                      <p>After gaining its independence, Bangladesh became a republic within the Commonwealth and a secular democracy. in 1973, Bangladesh joined the OIC and the Non-Aligned Movement, and later joined the United Nations in 1974.</p>
                      <p>In 1973, Bangladesh, Pakistan and India signed a tripartite agreement calling for stability and peace in the subcontinent. The government at that time was led by Mujib who was assassinated together with the majority of his family members on 15 August 1975. In the same year, two army uprisings took place, and they led to a reorganized system of power.
                      </p>
                      <p>In 1977, Lieutenant General Ziaur Rahman took over the presidency and in 1979 he restored civilian rule and reinstated multi-party politics. He formed the Bangladesh National Party and promoted free markets. He also reinstated the country’s foreign policy and sought out closer ties with the West. His tenure ended in 1981 when he was assassinated by the military. His successor was Abdus Sattar, whose tenure ended in less than a year.
                      </p>
                      <p>Lieutenant General Hussain Muhammad Ershad was the country’s next big ruler and as president he pursued executive reforms, including a devolution scheme which partitioned the nation into 64 districts and 5 divisions. in 1985, he held the founding summit of SAARC in Dhaka, bringing together 7 South Asian nations, including the Maldives, Bangladesh, India, Bhutan, Sri Lanka, and Nepal, into a regional union. He also extended Bangladesh’s road network and began significant projects, for instance, the Jamuna Bridge. He reinstates civilian rule in 1986 and established the Jatiya Party.
                      </p>
                      <p>In 1991, former first lady Khaleda Zia led the Bangladesh Nationalists Party and was elected as the first female Prime Minister in the country’s history.
                      </p>
                      <p>The next general elections were held in 1996 and the Awami League led by Sheikh Hasina came back to power after 20 years. In 1999 the Sheikh  held a trilateral summit between Bangladesh, Pakistan, and India and helped form the D8 grouping with Turkey. The League, however, lost power again to the Bangladesh Nationalist Party (BNP) in the 2001 election and Khaleda Zia was once again elected as the Prime Minister.
                      </p>
                      <p>BNP’s tenure came to an end in October 2006 and a caretaker government led by President Iajuddin Ahmed worked to ensure that the parties would take part in the elections within ninety days. The Bangladesh Armed Forces intervened on 11 January 2007 so as to support a state of emergency and a neutral caretaker government under Chief Advisor Fakhruddin Ahmed. The Awani League won the 2008 general elections.
                      </p>
                      <p>
                          Over the years, Bangladesh has reduced poverty a lot with the rate lowering from 57 % in 1990 to 25.6% in 2014. Per-capita income has doubled and the country has acquired success in human development. Bangladesh, however, still faces challenges of climate change, inequality, religious extremism, and unstable politics.
                      </p>
                  </div>
              </div>
            </div>
        </section>

    </section>
    <section class="container-fluid">
        <div class=" history_link">
            <h3><a href="{{url('language')}}">Language, Culture & Religion of Bangladesh</a> </h3>
        </div>
    </section>
@endsection
