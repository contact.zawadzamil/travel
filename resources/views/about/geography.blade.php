@extends('layout')
@section('body_parts')
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="pageTitle">Home >> Geography and Climate of Bangladesh</h3>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <section class="section-padding">
            <div class="container">
                <div class="container">
                    <div class="row lccr">
                        <h3>Geography and Climate of Bangladesh</h3>
                        <div class="language inner_section">
                            <h4 class="sub_title">Geography</h4>
                            <p>
                                Bangladesh has an area of approximately 147,540 square kilometer in the south Asian region. The country is surrounded by India completely in the West, North, and partially in the East sharing a total of 4,053 kilometer border, while the rest 193 kilometer of the Eastern side is bordered by Myanmar. The Bay of Bengal retains its boundary In the South, where we have a 580 kilometer of coastline. About half the total area is actively deltaic and never higher than 10m from mean sea level. This flat low lying land is very fertile and is suitable for rice cultivation. The vast river delta area is home to the dominant plains culture. In the northeast and the southeast the land is more hilly and dry, and tea is grown. The hilly areas of the northeast and southeast are occupied by much smaller tribal groups. Ganges and Brahmaputra are the two main rivers of Bangladesh, carrying tones of silts from the mighty Himalayans that eventually fertile the plain. Apart from these two rivers, we have hundreds of others comprising a very wide and complex river system. Sundarbans, the largest mangrove forest of the world, is situated in the southwest. The Chittagong Hill Tracts have extensive hardwood forests. Lawachara is a semi-evergreen forest situated in the northeast in Sri Mangal. The Sal forest is spread around in various parts of the country, like Bhawal and Modhupur National Park.
                            </p>
                        </div>
                        <div class="culture inner_section">
                            <h4 class="sub_title">Climate</h4>
                            <p>
                                Bangladesh has tropical monsoon climate characterized by wide seasonal variations in rainfall, high temperatures, and high humidity. Regional climatic differences in this flat country are minor. Three seasons are generally recognized: a hot, muggy summer from March to June; a hot, humid and rainy monsoon season from June to November; and a warm-hot, dry winter from December to February. In general, maximum summer temperatures range between 38 and 41 °C (100.4 and 105.8 °F). April is the hottest month in most parts of the country. January is the coolest (but still hot) month, when the average temperature for most of the country is 16–20 °C (61–68 °F) during the day and around 10 °C (50 °F) at night. Winds are mostly from the north and northwest in the winter, blowing gently over the country. From March to May, violent thunderstorms, called northwesters, produce winds of up to 60 kilometers per hour (37.3 mph). Heavy rainfall is characteristic of Bangladesh that helps irrigation in the rice field during the burning months of June – August. About 80 % of Bangladesh’s rain falls during the monsoon season. Most parts of the country receive at least 2,300 mm (90.6 in) of rainfall per year, but because of its location just south of the foothills of the Himalayas, Sylhet in northeastern Bangladesh receives the greatest average precipitation. Annual rainfall in that region ranges between 3,280 and 4,780 mm (129.1 and 188.2 in).
                            </p>
                        </div>

                    </div>

                </div>
            </div>
        </section>

    </section>
    <section class="container-fluid">
        <div class=" history_link">

        </div>
    </section>
@endsection
