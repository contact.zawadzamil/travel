<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Western Multi purpose Bootstrap Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="http://webthemez.com" />
    <!-- css -->
    <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="public/assets/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="public/assets/css/flexslider.css" rel="stylesheet" />
    <link href="public/assets/css/style.css" rel="stylesheet" />
    <link href="public/assets/css/about.css" rel="stylesheet" />
    <link href="public/assets/css/packages.css" rel="stylesheet" />
    <link href="public/assets/css/whyUs.css" rel="stylesheet" />
    <link href="public/assets/css/contact.css" rel="stylesheet" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<div id="wrapper">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="pull-left hidden-xs">We are Hiring!!</p>
                    <p class="pull-right"><i class="fa fa-phone"></i>Tel No.(+001) 123-456-789</p>
                </div>
            </div>
        </div>
    </div>
    <!-- start header -->
    <header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('')}}"><img src="public/assets/img/logo.png" alt="logo"/></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="{{url('')}}">Home</a></li>
                        <li class="dropdown ">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Know <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('meet')}}">Meet Bangladesh</a></li>
                                <li><a href="{{url('history')}}">History of Bangladesh</a></li>
                                <li><a href="{{url('language')}}">Language, Culture & Religion of Bangladesh</a></li>
                                <li><a href="{{url('geography')}}">Geography and Climate of Bangladesh</a></li>
                            </ul>
                        </li>
                        <li><a href="{{url('tour_packages')}}">Tour Packages</a></li>
                        <li><a href="{{url('services')}}">Services</a></li>
                        <li><a href="{{url('why-us')}}">Why Us</a></li>
                        <li><a href="{{url('contact')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- end header -->


    @yield('body_parts')



    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="widget">
                        <h5 class="widgetheading">Our Contact</h5>
                        <address>
                            <strong>Bootstrap company Inc</strong><br>
                            JC Main Road, Near Silnile tower<br>
                            Pin-21542 NewYork US.</address>
                        <p>
                            <i class="icon-phone"></i> (123) 456-789 - 1255-12584 <br>
                            <i class="icon-envelope-alt"></i> email@domainname.com
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="#">Latest Events</a></li>
                            <li><a href="#">Terms and conditions</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="widget">
                        <h5 class="widgetheading">Latest posts</h5>
                        <ul class="link-list">
                            <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                            <li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
                            <li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="widget">
                        <h5 class="widgetheading">Recent News</h5>
                        <ul class="link-list">
                            <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                            <li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
                            <li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="copyright">
                            <p>
                                <span>&copy; Bootstrap Template 2018 All right reserved. Template By </span><a href="http://webthemez.com/free-bootstrap-templates/" target="_blank">WebThemez</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <ul class="social-network">
                            <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="public/assets/js/jquery.js"></script>
<script src="public/assets/js/jquery.easing.1.3.js"></script>
<script src="public/assets/js/bootstrap.min.js"></script>
<script src="public/assets/js/jquery.fancybox.pack.js"></script>
<script src="public/assets/js/jquery.fancybox-media.js"></script>
<script src="public/assets/js/jquery.flexslider.js"></script>
<script src="public/assets/js/animate.js"></script>
<!-- Vendor Scripts -->
<script src="public/assets/js/modernizr.custom.js"></script>
<script src="public/assets/js/jquery.isotope.min.js"></script>
<script src="public/assets/js/jquery.magnific-popup.min.js"></script>
<script src="public/assets/js/animate.js"></script>
<script src="public/assets/js/custom.js"></script>
<script src="public/assets/js/main.js"></script>
</body>
</html>
