@extends('layout')
@section('body_parts')
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle">Our Services</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container content">
            <!-- Service Blcoks -->
           <div class="row">
               <h3>Filmmaking Support in Bangladesh</h3>
               <p>
                   Pathfriend Tours offering you the best support for any kind of filmmaking in Bangladesh. We have worked for some professional films including feature film, Documentary film and Wildlife projects in around Bangladesh. The supports for the filmmaking we provide are follwing:
               </p>
               <ul class="service_list">

                   <li>Permissions</li>
                   <li>Camera Rental: ARRI, RED and other filmmaking pro Camera.</li>
                   <li>Location Management</li>
                   <li>Accommodation</li>
                   <li>Transportation</li>
                   <li>Food</li>
                   <li>Logistics Support: Track, Steady Cam, Crane, Drone & others equipments</li>
                   <li>Production Manager & Support</li>
               </ul>
           </div>




            <!-- End Service Blcoks -->




        </div>
    </section>
@endsection
