@extends('layout')
@section('body_parts')
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle">Why Us</h2>
                </div>
            </div>
        </div>
    </section>
{{--    Award Section --}}
    <section id="content" class="container-fluid awardSection">
        <div class="container content award">
          <div class="row">
              <div class="col-md-4">
                  <div class="image">
                      <img src="{{asset('storage/images/2021-winners-badge-PNG.png')}}" alt="">
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="image narrow">
                      <img src="{{asset('storage/images/world-travel-awards-nominee-2019.png')}}" alt="">
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="image narrow">
                      <img src="{{asset('storage/images/WTA-Winner-2020-logo-copy.png')}}" alt="">
                  </div>
              </div>
          </div>
            <div class="row describe">
                <p> <strong>Western Tour Operator</strong> is a leading local tour operators in Bangladesh, committed to give you a lifetime Bangladesh experience. As Bangladesh is full of natural beauty and hidden treasure, its underdeveloped infrastructure can disrupt your travel plans. And this is why we are here to make your trip perfect with your needs. Our tour planning, time management, best accommodation, good quality food, local experience, freedom to blend in with this country and knowledgeable guides will impress you. Whether you are in Bangladesh for a private tour or a group tour, we can be a new and unique experience for your trip.</p>
            </div>




        </div>
    </section>

{{--    Why Us Title --}}
    <section class="container-fluid whyUs">
        <h2 >Why Us?</h2>
    </section>

{{--    Why Us Detailes --}}
    <section class="whyUs-description">
        <div class="container">
            <div class="row">
                <div class="col-md-6 ">
                    <div class="why-card">
                        <div class="icon">
                            <img src="{{asset('storage/images/icons/booking.png')}}" alt="">
                        </div>
                        <div class="title">
                            <h4>Free Cancellation & Booking Policy</h4>
                        </div>
                        <div class="described">
                            <p>
                                We offer free cancellation & booking Policy for any tour. In some cases, you can cancel or book the tour on the day before it starts. Please check our Booking & Cancellation Policy to know more about each tour.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="why-card">
                        <div class="icon">
                            <img src="{{asset('storage/images/icons/shield.png')}}" alt="">
                        </div>
                        <div class="title">
                            <h4>Safe & Sustainable Travel</h4>
                        </div>
                        <div class="described">
                            <p>
                                Your next travel should be safe & sustainable after the COVID-19 outbreak. We assure safety on every step maintaining rules awareness and offering healthy food. We focus on sustainability of nature and eco-friendly travel.


                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="why-card">
                        <div class="icon">
                            <img src="{{asset('storage/images/icons/badge.png')}}" alt="">
                        </div>
                        <div class="title">
                            <h4>High Quality Service Assurance</h4>
                        </div>
                        <div class="described">
                            <p>
                                We assure the quality services for every single trip with best tour plan, high rated accommodations, transportations and quality food. Our full time knowledgeable and accompanied guide are committed to make it perfect.


                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="why-card">
                        <div class="icon">
                            <img src="{{asset('storage/images/icons/store.png')}}" alt="">
                        </div>
                        <div class="title">
                            <h4>Veteran Local Agency</h4>
                        </div>
                        <div class="described">
                            <p>
                                Our experiences of many years in tourism  will  pleasure you by its nature. Through the good relationship with the locality and the authority, our services are highly occupied, travel-friendly, 100% secured and conductive.

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="why-card">
                        <div class="icon">
                            <img src="{{asset('storage/images/icons/local.png')}}" alt="">
                        </div>
                        <div class="title">
                            <h4>Local Experiences</h4>
                        </div>
                        <div class="described">
                            <p>

                                Our package focus on the local places, people & culture. Traditional attractions, foods, rides and festivals are highly prior to the itinerary to present a real Bangladeshi way of living.

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="why-card">
                        <div class="icon">
                            <img src="{{asset('storage/images/icons/teamwork.png')}}" alt="">
                        </div>
                        <div class="title">
                            <h4>Special Group Tour Offer </h4>
                        </div>
                        <div class="described">
                            <p>
                                We offering best rate for your group tour of any number. Firstly, we prefer a private arrangement for a trip to make it comfortable and enjoyable with our extraordinary services.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="why-card">
                        <div class="icon">
                            <img src="{{asset('storage/images/icons/spy.png')}}" alt="">
                        </div>
                        <div class="title">
                            <h4> Private & Small Group</h4>
                        </div>
                        <div class="described">
                            <p>
                                We offer the best way to make your trip private and comfortable. Any number of travelers are welcome to have a great experience of visiting in your own way. But if anyone wants to join in a package, we have a scheduled tour with maximum group of 6 in any destination.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="why-card">
                        <div class="icon">
                            <img src="{{asset('storage/images/icons/handshake.png')}}" alt="">
                        </div>
                        <div class="title">
                            <h4> Best Price Deal</h4>
                        </div>
                        <div class="described">
                            <p>
                                We guaranty the lower price for every single trip. Good working relationships with direct local owners of properties or services allowed us to give you a lower price.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    {{--    Our Motive --}}
    <section class="container-fluid motive">
        <h2 >Our Motive</h2>
    </section>

{{--    Motive Details --}}
    <section class="motive-details">
        <div class="container">
            <div class="row">
                <p>
                    We are fully experienced in any private and group tour in Bangladesh offer a solution for visiting Bangladesh to make your travel pleasant. We focus on meeting expectations & satisfaction of every guest visiting our country and try to help you organize your stay in the best way.
                </p>

                <p>Our unique and custom tours are designed to give you the opportunity to see Bangladesh from unusual viewpoints. Through our website we try to supply you with as much information as we can to give you opportunity to check all possibilities before making any travel arrangements. We are continually developing the travel guide section so you can find here all the information you need before coming to Bangladesh.</p>

                <p>Our aim is to make your stay in Bangladesh an unforgettable experience. We hope you will decide to ask us for help and we look forward to being at your service. We also invite to cooperation travel agencies from all over the world who wish to supply work with high-quality services in Bangladesh.</p>
            </div>
        </div>
    </section>
@endsection
