<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Get ALl Types
Route::get('get_types',[\App\Http\Controllers\TypeController::class,'index']);

// Get ALl Packages
Route::get('get_packages',[\App\Http\Controllers\PackageController::class,'getPackages']);


//Get Duration
Route::get('duration/{id}',[\App\Http\Controllers\PackageController::class,'getDuration']);

//Swagger Routes
Route::get('swagger',[\App\Http\Controllers\SwaggerController::class,'index']);
Route::post('add_type',[\App\Http\Controllers\SwaggerController::class,'storeType']);
Route::post('edit_type/{id}',[\App\Http\Controllers\SwaggerController::class,'editType']);

