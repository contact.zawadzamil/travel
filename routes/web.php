<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Frontend Routes

Route::get('/','\App\Http\Controllers\HomeController@index');
Route::get('details/{id}','\App\Http\Controllers\HomeController@details')->name('home.details');
Route::get('about', function () {
    return view('about');
});

// Tour Packages
Route::get('tour_packages','\App\Http\Controllers\DashboardController@getPackages');

Route::get('portfolio', function () {
    return view('portfolio');
});
Route::get('pricing', function () {
    return view('pricing');
});
Route::get('contact', function () {
    return view('contact');
});
Route::get('services', function () {
    return view('services');
});
Route::get('why-us', function () {
    return view('why-us');
});
//Know
Route::get('meet', function () {
    return view('about.meet-bd');
});
Route::get('history', function () {
    return view('about.history');
});
Route::get('language', function () {
    return view('about.language');
});
Route::get('geography', function () {
    return view('about.geography');
});


// Message
Route::post('message','\App\Http\Controllers\MessageController@store')->name('message.store');
Route::get('all-messages','\App\Http\Controllers\MessageController@index')->name('message.index');
Route::get('delete-message/{id}','\App\Http\Controllers\MessageController@destroy')->name('message.delete');




Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
Route::get('/dashboard','\App\Http\Controllers\DashboardController@adminDashboard')->middleware(['auth']);
Route::get('admin','\App\Http\Controllers\DashboardController@adminDashboard')->middleware(['auth'])->name('admin');

// Package Routes
Route::post('store_package','\App\Http\Controllers\PackageController@store')->name('package_store');
Route::get('packages','\App\Http\Controllers\PackageController@index')->middleware(['auth'])->name('packages');
Route::get('edit_package/{id}','\App\Http\Controllers\PackageController@edit')->middleware(['auth'])->name('package.edit');
Route::post('update-package/{id}','\App\Http\Controllers\PackageController@update')->name('package.update')->middleware(['auth']);
Route::get('single-package/{id}','\App\Http\Controllers\PackageController@show')->name('package.single')->middleware(['auth']);
Route::get('delete-package/{id}','\App\Http\Controllers\PackageController@destroy')->name('package.delete')->middleware(['auth']);


// Types Routes
Route::get('types','\App\Http\Controllers\TypeController@indexWeb')->name('type.index-web')->middleware(['auth']);
Route::get('add-types','\App\Http\Controllers\TypeController@create')->name('type.create')->middleware(['auth']);
Route::post('store_type','\App\Http\Controllers\TypeController@store')->name('type.store')->middleware(['auth']);
Route::get('edit-type/{id}','\App\Http\Controllers\TypeController@edit')->name('type.edit')->middleware(['auth']);
Route::post('update-type/{id}','\App\Http\Controllers\TypeController@update')->name('type.update')->middleware(['auth']);
Route::get('delete-type/{id}','\App\Http\Controllers\TypeController@destroy')->name('type.destroy')->middleware(['auth']);

// Gallery Routes
Route::get('add-gallery','\App\Http\Controllers\GalleryController@create')->name('gallery.create')->middleware('auth');;
Route::post('store-gallery','\App\Http\Controllers\GalleryController@store')->name('gallery.store')->middleware('auth');;
Route::get('view-gallery','\App\Http\Controllers\GalleryController@index')->name('gallery.index')->middleware('auth');;
Route::get('remove-gallery/{id}','\App\Http\Controllers\GalleryController@destroy')->name('gallery.destroy')->middleware('auth');;


// Schedule Routes
Route::get('add-schedule','App\Http\Controllers\ScheduleController@create')->name('schedule.create')->middleware('auth');;
Route::post('store-schedule','App\Http\Controllers\ScheduleController@store')->name('schedule.store')->middleware('auth');;
Route::get('schedules','App\Http\Controllers\ScheduleController@index')->name('schedule.index')->middleware('auth');;
Route::get('edit-schedule/{id}','App\Http\Controllers\ScheduleController@edit')->name('schedule.edit');
Route::post('update-schedule/{id}','App\Http\Controllers\ScheduleController@update')->name('schedule.update')->middleware('auth');;
Route::get('delete-schedule/{id}','App\Http\Controllers\ScheduleController@destroy')->name('schedule.destroy')->middleware('auth');;


//Route::group(['middleware' => ['auth']], function() {
//    Route::resource('roles', RoleController::class);
//    Route::resource('users', UserController::class);
//});

//Roles

Route::get('roles','App\Http\Controllers\RoleController@index')->name('role.index')->middleware('auth');
Route::get('roles-create','App\Http\Controllers\RoleController@create')->name('role.create')->middleware('auth');
Route::post('roles-store','App\Http\Controllers\RoleController@store')->name('role.store')->middleware('auth');


//Users
Route::get('users','App\Http\Controllers\UserController@index')->name('user.index')->middleware('auth');
Route::get('users-create','App\Http\Controllers\UserController@create')->name('user.create')->middleware('auth');
Route::post('users-store','App\Http\Controllers\UserController@store')->name('user.store')->middleware('auth');

// Social Login Authes
//Google
Route::get('auth/google','App\Http\Controllers\GoogleAuthController@redirect')->name('google.auth');
Route::get('auth/google/callback/','App\Http\Controllers\GoogleAuthController@callbackGoogle');

Route::post('password_add','App\Http\Controllers\GoogleAuthController@savePassword')->name('password_store');

// Facebook
Route::get('auth/facebook','App\Http\Controllers\FacebookAuthController@redirect')->name('facebook.auth');
Route::get('auth/facebook/callback/','App\Http\Controllers\FacebookAuthController@callbackFacebook');

//Github
Route::get('auth/github','App\Http\Controllers\GithubAuthController@redirect')->name('github.auth');
Route::get('auth/github/callback/','App\Http\Controllers\GithubAuthController@callbackGithub');



//Push Notification
Route::get('push-index','App\Http\Controllers\DashboardController@pushIndex')->name('push-index');
Route::post('push-message','App\Http\Controllers\EventController@pushTestMessage')->name('push-test-message');
// Auth Routes




Route::get('add_package', function () {
    return view('admin.add_package');
})->middleware(['auth'])->name('packages.add');

require __DIR__.'/auth.php';
